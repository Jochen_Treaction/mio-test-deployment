# README #
## VERSION:2.0.3.0 ##
# INSTALLATION / DEPLOYMENT TEST-SYSTEM #

This README documents whatever steps are necessary to get applications on **MARKETING-IN-ONE TEST-SYSTEM** up and running.

### What is this repository for? ###

* This repo contains installation shell scripts for test-marketing-in-one.net and test-api-in-one.net and their subdomains
* Version 1.0, 2021-08-23

### IMPORTANT ###
This Installation uses *PHP 7.4.22*

Web-Server is *Apache 2.4*

**Symfony Versions used are** 

*V4.4.29* (test-markteting-in-one.net, test-api-in-one.net, msmio.test-api-in-one.net)

*V5.3.6* (mscamp.test-marketing-in-one.net, mshb.test-api-in-one.net,msmail.test-api-in-one.net,mswebinaris.test-api-in-one.net)

*V5.1.11* (management.test-api-in-one.net) => *should be raised to 5.3.6*

*V5.0.11* (msdigi.test-api-in-one.net)  => *should be raised to 5.3.6*


### Important Directories ###

**Root directory** of each domain / subdomain is

`<domainname>/httpdocs/public`

**Clone directory** of each domain / subdomain is

`<domainname>/clone/<repo>`

### Important files ###

Each `<domainname>|<subdomainname>/httpdocs/public` directory contains two files
* **.env** or corresponding 
* **composer.json**

####  !!! NOTE !!! ####
Both files are usually write protected. Versions of these files are controlled by Bitbucket. 

BUT: NEITHER .env NOR composer.json WILL BE INSTALLED / OVERWRITTEN BY ANY `install*.sh`-script!

To make changes in .env or composer.json, do changes first (by eventually choosing a different, but corresponding name => following symfony rules and standards!!!) in Bitbucket.

After that do and test your changes on the TEST-SYSTEM in the corresponding folder.


### Installation shell scripts ###
#### install applications and microservices ####
Installation shell scripts are placed below each `<domain>|<subdomain>`-directory
* ./msmio.test-api-in-one.net/**install_msmio.sh**
* ./management.test-api-in-one.net/**install-management.sh**
* ./msmail.test-api-in-one.net/**install_msmail.sh**
* ./mshb.test-api-in-one.net/**install_mshb.sh**
* ./mswebinaris.test-api-in-one.net/**install_mswebinaris.sh**
* ./**install-aoi.sh**
* ./msdigi.test-api-in-one.net/**install_msdigi.sh**

* ./mscamp.test-marketing-in-one.net/**install-mscamp.sh**
* *(./install_marketing-in-one-dev.sh)*
* ./**install_marketing-in-one-testmio.sh**

#### How to install campaings.marketing-in-one.net #####
Simply log in into Plesk go to Websites/Domains, choose campaings.marketing-in-one.net and click `[Bereitstellen]` or `[Updates mit Hilfe von Pull abrufen]`. Then check the filesystem `campaings.marketing-in-one.net/httpdocs/public`, if your changes / updates are done.



#### Run an installation ####
Logon to *API-IN-ONE* with user `treaction-kto206-api`.

Logon to *MARKETING-IN-ONE* with user `sysuser_2`.

Find passwords in KEEPASS.

Go to the `<domain>|<subdomain>`-directory and run the corresponding script by `sh <install>*.sh` to install the latest (in future TAG-ed) version of the APP you wish to install.


### Basic Installation Tests ###
Import **TEST-SYSTEM.postman_collection.json** tro your Postman and run the correponding requests to check if your installed APP / MICROSERVICE responds.



### Change a deployment / installation script ###
If you need to modify scripts, you **MUST** modify them first in the **version control system** (see https://bitbucket.org/Jochen_Treaction/mio-test-deployment/src/master/) before you change the original one on the system.

**DO NOT FORGET TO ADD EXPLAINING COMMENTS TO YOUR CHANGES IN THE FILE AND IN BITBUCKETS COMMIT COMMENT.**

To clone the TEST-SYSTEM scripts to your work directory use: `git clone https://Jochen_Treaction@bitbucket.org/Jochen_Treaction/mio-test-deployment.git`
