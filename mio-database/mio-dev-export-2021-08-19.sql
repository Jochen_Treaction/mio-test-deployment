-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 19. Aug 2021 um 11:37
-- Server-Version: 10.1.48-MariaDB-0ubuntu0.18.04.1
-- PHP-Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `mio`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `action`
--

CREATE TABLE `action` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `method` varchar(10) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `end_point` varchar(255) DEFAULT NULL,
  `params` varchar(255) DEFAULT NULL,
  `output_status` varchar(255) DEFAULT NULL,
  `output_message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `action`
--

INSERT INTO `action` (`id`, `name`, `method`, `host`, `end_point`, `params`, `output_status`, `output_message`) VALUES
(1, 'send_lead_notification', 'POST', 'AIO', '/processhook/send/leadnotification', '{\'email\':\'value\', \'contact_data\' : [\'key1\':\'value1\', \'key2\':\'value2\']}', 'bool', '{\'status\':\'0|1\' , \'\'error\': \'error msg txt\', \'success\': \'success msg txt\'}'),
(2, 'start_marketing_automation', 'POST', 'AIO', '/emio/trigger/marketing_automation', '{\'email\':\'value\', \'apikey\':\'key\',\'ma_id\': \'id\'}', 'bool', '{\'status\':\'0|1\' , \'\'error\': \'error msg txt\', \'success\': \'success msg txt\'}'),
(3, 'send_double_opt_in_mail', 'POST', 'AIO', '/emio/trigger/doi_mail', '{\'email\':\'value\', \'apikey\':\'key\', \'doi_id\': \'id\'}', 'bool', '{\'status\':\'0|1\' , \'\'error\': \'error msg txt\', \'success\': \'success msg txt\'}'),
(4, 'trigger_contact_event', 'POST', 'AIO', '/emio/trigger/contact_event', '{\'email\':\'value\', \'apikey\':\'key\', \'ce_id\': \'id\'}', 'bool', '{\'status\':\'0|1\' , \'\'error\': \'error msg txt\', \'success\': \'success msg txt\'}'),
(5, 'double_opt_in_mail_xor_contact_event', 'POST', 'AIO', '/emio/trigger/contact_event_xor_doi_mail', '{\'email\':\'value\', \'apikey\':\'key\', \'ce_id\': \'id\', \'doi_id\':\'id\'}', 'bool', '{\'status\':\'0|1\' , \'\'error\': \'error msg txt\', \'success\': \'success msg txt\'}'),
(6, 'set_smart_tag', 'POST', 'AIO', '/emio/set/smart_tag', '{\'email\':\'value\', \'apikey\':\'key\', \'tags\': [\'tag1\', \'tag2\', \'tag3\']}', 'bool', '{\'status\':\'0|1\' , \'\'error\': \'error msg txt\', \'success\': \'success msg txt\'}'),
(7, 'remove_smart_tag', 'POST', 'AIO', '/emio/remove/smart_tag', '{\'email\':\'value\', \'apikey\':\'key\', \'tags\': [\'tag1\', \'tag2\', \'tag3\']}', 'bool', '{\'status\':\'0|1\' , \'\'error\': \'error msg txt\', \'success\': \'success msg txt\'}'),
(8, 'show_popup', 'GET', 'POPUP', '/popup/read', '{\'popupId\': \'id\' }', 'bool', '{\'status\':\'0|1\', \'error\': \'error msg txt\', \'success\': \'success msg txt\', \'popuphtml\':\'\'}');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `administrates_companies`
--

CREATE TABLE `administrates_companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `administrates_companies`
--
DELIMITER $$
CREATE TRIGGER `administrates_companies_BEFORE_INSERT` BEFORE INSERT ON `administrates_companies` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `administrates_companies_BEFORE_UPDATE` BEFORE UPDATE ON `administrates_companies` FOR EACH ROW BEGIN
set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `administrates_users`
--

CREATE TABLE `administrates_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `invited_user_id` bigint(20) UNSIGNED NOT NULL,
  `to_company_id` bigint(20) UNSIGNED NOT NULL,
  `with_roles` text NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `administrates_users`
--
DELIMITER $$
CREATE TRIGGER `administrates_users_BEFORE_INSERT` BEFORE INSERT ON `administrates_users` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `administrates_users_BEFORE_UPDATE` BEFORE UPDATE ON `administrates_users` FOR EACH ROW BEGIN
set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `blacklist_domains`
--

CREATE TABLE `blacklist_domains` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `domain_name` varchar(255) NOT NULL COMMENT 'Blacklisted Domain names',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `blacklist_domains`
--
DELIMITER $$
CREATE TRIGGER `blacklist_domains_BEFORE_INSERT` BEFORE INSERT ON `blacklist_domains` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `blacklist_domains_BEFORE_UPDATE` BEFORE UPDATE ON `blacklist_domains` FOR EACH ROW BEGIN
set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `campaign`
--

CREATE TABLE `campaign` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `objectregister_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL,
  `account_no` varchar(40) DEFAULT '',
  `url` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `campaign`
--
DELIMITER $$
CREATE TRIGGER `campaign_BEFORE_INSERT` BEFORE INSERT ON `campaign` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `campaign_BEFORE_UPDATE` BEFORE UPDATE ON `campaign` FOR EACH ROW BEGIN
set new.updated = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `clientside_pagetypes`
--

CREATE TABLE `clientside_pagetypes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `processhook_id` bigint(20) UNSIGNED NOT NULL,
  `pagetype` varchar(45) NOT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `clientside_pagetypes`
--

INSERT INTO `clientside_pagetypes` (`id`, `processhook_id`, `pagetype`, `comment`) VALUES
(1, 1, 'form#leadnotification', 'Lead Notification Form'),
(2, 2, 'form#leadnotification', 'Lead Notification Form'),
(3, 2, 'body.custom', 'Custom Page'),
(4, 1, 'body.survey', 'Survey / Questioniare'),
(5, 1, 'body.success', 'Success Page');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `company`
--

CREATE TABLE `company` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `objectregister_id` bigint(20) UNSIGNED NOT NULL,
  `account_no` varchar(100) NOT NULL,
  `name` text,
  `street` text,
  `house_number` text,
  `postal_code` text,
  `city` text,
  `country` text,
  `website` text,
  `phone` text,
  `vat` text,
  `login_url` text,
  `delegated_domain` text COMMENT 'Only when URL option is delegated domain',
  `url_option` varchar(255) DEFAULT 'folder' COMMENT 'default was folder(must be removed for TEXT) , subdomain, deligate domain',
  `hash_name` varchar(128) DEFAULT NULL,
  `hash_street` varchar(128) DEFAULT NULL,
  `hash_house_number` varchar(128) DEFAULT NULL,
  `hash_postal_code` varchar(128) DEFAULT NULL,
  `hash_city` varchar(128) DEFAULT NULL,
  `hash_country` varchar(128) DEFAULT NULL,
  `hash_website` varchar(128) DEFAULT NULL,
  `hash_phone` varchar(128) DEFAULT NULL,
  `hash_vat` varchar(128) DEFAULT NULL,
  `hash_login_url` varchar(128) DEFAULT NULL,
  `hash_delegated_domain` varchar(128) DEFAULT NULL,
  `modification` bigint(20) UNSIGNED DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `company`
--
DELIMITER $$
CREATE TRIGGER `company_BEFORE_INSERT` BEFORE INSERT ON `company` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `company_BEFORE_UPDATE` BEFORE UPDATE ON `company` FOR EACH ROW BEGIN
set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `customer_journey`
--

CREATE TABLE `customer_journey` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `lead_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Lead id from leads',
  `customer_journey` varchar(255) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `customer_journey`
--
DELIMITER $$
CREATE TRIGGER `customer_journey_BEFORE_INSERT` BEFORE INSERT ON `customer_journey` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `customer_journey_BEFORE_UPDATE` BEFORE UPDATE ON `customer_journey` FOR EACH ROW BEGIN
set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `customfields`
--

CREATE TABLE `customfields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ref_customfields_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'reference to project customfields for page and webhook customfields - loosely coupled - no foreign key defined',
  `objectregister_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'must be not null later on delete cascade',
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `datatypes_id` bigint(20) UNSIGNED NOT NULL,
  `fieldname` varchar(80) NOT NULL DEFAULT 'undefined',
  `hide_in_ui_selection` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'can be set by user in project-level UI to leave this field active for running campaigns but disable it for selection in future pages or webhooks',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `regexp_validation` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `datainterface_fields`
--

CREATE TABLE `datainterface_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source_objectregister_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'objectregister_id of unique_object_type customfield,  lead or (currently not existing) standardfields',
  `output_datainterface_id` bigint(20) UNSIGNED DEFAULT NULL,
  `input_datainterface_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `datatypes`
--

CREATE TABLE `datatypes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(80) NOT NULL COMMENT 'for example string, int, email, double, boolean, ...',
  `phptype` varchar(80) DEFAULT NULL,
  `base_regex` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `datatypes`
--

INSERT INTO `datatypes` (`id`, `name`, `phptype`, `base_regex`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Date', 'date', '^d{2}(.|-)d{2}(.|-)d{4}$', NULL, NULL, NULL, NULL),
(2, 'DateTime', 'date', '^d{2}(.|-)d{2}(.|-)d{4} d{2}:d{2}:d{2}$', NULL, NULL, NULL, NULL),
(3, 'Decimal', 'double', '^[d]*[.]?[d]*$', NULL, NULL, NULL, NULL),
(4, 'Email', 'string', '^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-z]{2,9})$', NULL, NULL, NULL, NULL),
(5, 'Integer', 'integer', '^[0-9]+$', NULL, NULL, NULL, NULL),
(6, 'List', 'string', '([w-.]+,?)*', NULL, NULL, NULL, NULL),
(7, 'Phone', 'string', '[0-9-/ ()+]*', NULL, NULL, NULL, NULL),
(8, 'Text', 'string', '.*', NULL, NULL, NULL, NULL),
(9, 'URL', 'string', '^(?:(?:https?)://)(?:S+(?::S*)?@|d{1,3}(?:.d{1,3}){3}|(?:(?:[a-zdx{00a1}-x{ffff}]+-?)*[a-zdx{00a1}-x{ffff}]+)(?:.(?:[a-zdx{00a1}-x{ffff}]+-?)*[a-zdx{00a1}-x{ffff}]+)*(?:.[a-zx{00a1}-x{ffff}]{2,6}))(?::d+)?(?:[^s]*)?$', NULL, NULL, NULL, NULL),
(10, 'Boolean', 'boolean', '^(true|false)$', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `event`
--

CREATE TABLE `event` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ui_name` varchar(255) NOT NULL,
  `attached_dom_element` varchar(255) NOT NULL,
  `attached_js_event` varchar(255) NOT NULL,
  `comment` text COMMENT 'add as comment what the fuck that bnutton is: button [submit] or button[custom1], button[custom2], button[custom3]'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `event`
--

INSERT INTO `event` (`id`, `ui_name`, `attached_dom_element`, `attached_js_event`, `comment`) VALUES
(1, 'load_page', 'window', 'load', '$(window).on(\"load\", function () {} => triggered after all assets on the page have loaded, including images. OR $( document ).ready( handler ) => as soon as the page\'s Document Object Model (DOM) becomes safe to manipulate'),
(2, 'leave_page', 'window', 'unload', '$(window).on(\"unload\", function(){} => sent to the window element when the user navigates away from the page'),
(3, 'change_tab', 'any', 'blur', '$(any element).on(\"blur\", function(){} => ent to an element when it loses focus'),
(4, 'submit', 'button', 'click', '$(\"button#anyid\").on(\"click\", function(){}'),
(5, 'close_page', 'window', 'unload', 'Same as LeavePage => $(window).on(\"unload\", function(){} => sent to the window element when the user navigates away from the page');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fraud`
--

CREATE TABLE `fraud` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Campaign Id ',
  `type` varchar(255) DEFAULT NULL COMMENT 'IP Block, Blacklist Domain, Fraud Limit',
  `email` varchar(255) NOT NULL COMMENT 'Client email id',
  `domain` varchar(255) DEFAULT NULL COMMENT 'Client Domain name',
  `ip` varchar(100) DEFAULT NULL COMMENT 'Client Ip address',
  `device_browser` varchar(255) DEFAULT NULL COMMENT 'Client Browser',
  `device_os` varchar(255) DEFAULT NULL COMMENT 'Client OS name ',
  `time_stamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Current time stamp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `fraud`
--
DELIMITER $$
CREATE TRIGGER `fraud_BEFORE_INSERT` BEFORE INSERT ON `fraud` FOR EACH ROW BEGIN
set new.time_stamp = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `option_id` bigint(20) UNSIGNED DEFAULT NULL,
  `imgtype` varchar(10) DEFAULT NULL,
  `img` longtext,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `images`
--
DELIMITER $$
CREATE TRIGGER `images_BEFORE_INSERT` BEFORE INSERT ON `images` FOR EACH ROW BEGIN
    set new.created = now();
    set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `images_BEFORE_UPDATE` BEFORE UPDATE ON `images` FOR EACH ROW BEGIN
    set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `input_datainterface`
--

CREATE TABLE `input_datainterface` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `data_source` varchar(255) DEFAULT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `integrations`
--

CREATE TABLE `integrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `objectregister_id` bigint(20) UNSIGNED NOT NULL,
  `configuration_target_id` bigint(20) UNSIGNED DEFAULT NULL,
  `company_id` bigint(20) UNSIGNED DEFAULT NULL,
  `campaign_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `integrations`
--
DELIMITER $$
CREATE TRIGGER `integrations_BEFORE_INSERT` BEFORE INSERT ON `integrations` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `integrations_BEFORE_UPDATE` BEFORE UPDATE ON `integrations` FOR EACH ROW BEGIN
set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `layout_type`
--

CREATE TABLE `layout_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status_txt` varchar(45) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'decoupled cio users id',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'decoupled cio users id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='VERSION 1-1 status TABLE BY JSR - REPLACES ENUM-TYPES IN FUTURE VERSIONS';

--
-- Trigger `layout_type`
--
DELIMITER $$
CREATE TRIGGER `status_BEFORE_DELETE` BEFORE DELETE ON `layout_type` FOR EACH ROW BEGIN
signal sqlstate '45003' set message_text=" A STATUS SHOULD NOT BE DELETED. IT CAN BE DELETED AFTER DISABLING THE BEFORE DELETE TRIGGER OF THIS TABLE!";
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `status_BEFORE_INSERT` BEFORE INSERT ON `layout_type` FOR EACH ROW BEGIN
set new.created_at = NOW();
set new.updated_at = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `status_BEFORE_UPDATE` BEFORE UPDATE ON `layout_type` FOR EACH ROW BEGIN
set new.updated_at = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `leads`
--

CREATE TABLE `leads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `objectregister_id` bigint(20) UNSIGNED NOT NULL,
  `account` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'account = company_id - shoud have a FK to company.id',
  `email` text NOT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` text,
  `last_name` text,
  `firma` text,
  `street` text,
  `postal_code` text,
  `city` text,
  `country` text,
  `phone` text,
  `campaign` text,
  `lead_reference` text,
  `url` text,
  `final_url` text,
  `trafficsource` text,
  `utm_parameters` text,
  `split_version` text,
  `targetgroup` text,
  `affiliateID` text COMMENT 'Affiliate Id from campaign URL',
  `affiliateSubID` text COMMENT 'AffiliateSubId from campaign URL',
  `ip` varchar(255) DEFAULT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `device_browser` varchar(255) DEFAULT NULL,
  `device_os` varchar(255) DEFAULT NULL,
  `device_os_version` varchar(255) DEFAULT NULL,
  `device_browser_version` varchar(255) DEFAULT NULL,
  `hash_email` varchar(128) NOT NULL COMMENT 'needed for fast, index based selection based on hashed for selection) email-adress',
  `hash_salutation` varchar(128) DEFAULT NULL,
  `hash_first_name` varchar(128) DEFAULT NULL,
  `hash_last_name` varchar(128) DEFAULT NULL,
  `hash_firma` varchar(128) DEFAULT NULL,
  `hash_street` varchar(128) DEFAULT NULL,
  `hash_postal_code` varchar(128) DEFAULT NULL,
  `hash_city` varchar(128) DEFAULT NULL,
  `hash_country` varchar(128) DEFAULT NULL,
  `hash_phone` varchar(128) DEFAULT NULL,
  `hash_campaign` varchar(128) DEFAULT NULL,
  `hash_lead_reference` varchar(128) DEFAULT NULL,
  `hash_url` varchar(128) DEFAULT NULL,
  `hash_final_url` varchar(128) DEFAULT NULL,
  `hash_trafficsource` varchar(128) DEFAULT NULL,
  `hash_utm_parameters` varchar(128) DEFAULT NULL,
  `hash_split_version` varchar(128) DEFAULT NULL,
  `hash_targetgroup` varchar(128) DEFAULT NULL,
  `hash_affiliateID` varchar(128) DEFAULT NULL,
  `hash_affiliateSubID` varchar(128) DEFAULT NULL,
  `hash_ip` varchar(128) DEFAULT NULL,
  `hash_device_type` varchar(128) DEFAULT NULL,
  `hash_device_browser` varchar(128) DEFAULT NULL,
  `hash_device_os` varchar(128) DEFAULT NULL,
  `hash_device_os_version` varchar(128) DEFAULT NULL,
  `hash_device_browser_version` varchar(128) DEFAULT NULL,
  `modification` bigint(20) UNSIGNED DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Trigger `leads`
--
DELIMITER $$
CREATE TRIGGER `leads_BEFORE_INSERT` BEFORE INSERT ON `leads` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `leads_BEFORE_UPDATE` BEFORE UPDATE ON `leads` FOR EACH ROW BEGIN
set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lead_answer_content`
--

CREATE TABLE `lead_answer_content` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `leads_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'can be null for anonymous survey answers as of web-4732',
  `questions_id` bigint(20) UNSIGNED NOT NULL,
  `lead_answer_content` longtext,
  `lead_answer_score` decimal(20,0) DEFAULT '0',
  `hash_lead_answer_content` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `lead_answer_content`
--
DELIMITER $$
CREATE TRIGGER `lead_answer_content_BEFORE_INSERT` BEFORE INSERT ON `lead_answer_content` FOR EACH ROW BEGIN
set new.created_at = NOW();
set new.updated_at = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_answer_content_BEFORE_UPDATE` BEFORE UPDATE ON `lead_answer_content` FOR EACH ROW BEGIN
set new.updated_at = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lead_customfield_content`
--

CREATE TABLE `lead_customfield_content` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `leads_id` bigint(20) UNSIGNED NOT NULL,
  `customfields_id` bigint(20) UNSIGNED NOT NULL,
  `lead_customfield_content` longtext COMMENT 'this field must be encrypted',
  `hash_lead_customfield_content` varchar(128) DEFAULT NULL COMMENT 'thats the hash of the unencypted lead_custom_field_content',
  `created` datetime DEFAULT NULL,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lead_sync_log`
--

CREATE TABLE `lead_sync_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `integration_id` bigint(20) UNSIGNED NOT NULL,
  `settings` mediumtext NOT NULL,
  `no_of_contacts_synchronized` int(11) DEFAULT NULL,
  `last_sync_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `last_sync_contact_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `message` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `miostandardfield`
--

CREATE TABLE `miostandardfield` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `datatypes_id` bigint(20) UNSIGNED NOT NULL,
  `fieldname` varchar(255) NOT NULL,
  `htmllabel` varchar(255) NOT NULL,
  `hidden_field` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `miostandardfield`
--

INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`, `hidden_field`) VALUES
(1, 4, 'email', 'Contact.Email', 0),
(2, 8, 'salutation', 'Contact.Salutation', 0),
(3, 8, 'first_name', 'Contact.FirstName', 0),
(4, 8, 'last_name', 'Contact.LastName', 0),
(5, 8, 'firma', 'Contact.Firma', 0),
(6, 8, 'street', 'Contact.Street', 0),
(7, 8, 'postal_code', 'Contact.PostalCode', 0),
(8, 8, 'city', 'Contact.City', 0),
(9, 8, 'country', 'Contact.Country', 0),
(10, 7, 'phone', 'Contact.Phone', 0),
(11, 8, 'lead_reference', 'Contact.LeadReference', 1),
(12, 9, 'url', 'Contact.Url', 0),
(13, 9, 'final_url', 'Contact.FinalUrl', 1),
(14, 8, 'trafficsource', 'Contact.TrafficSource', 1),
(15, 8, 'utm_parameters', 'Contact.UTMParameters', 1),
(16, 8, 'split_version', 'Contact.SplitVersion', 1),
(17, 8, 'targetgroup', 'Contact.Targetgroup', 1),
(18, 8, 'ip', 'Contact.IP', 1),
(19, 8, 'device_type', 'Contact.DeviceType', 1),
(20, 8, 'device_browser', 'Contact.DeviceBrowser', 1),
(21, 8, 'device_os', 'Contact.DeviceOs', 1),
(22, 8, 'device_os_version', 'Contact.DeviceOsVersion', 1),
(23, 8, 'device_browser_version', 'Contact.DeviceBrowserVersion', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `oauth2_tokens`
--

CREATE TABLE `oauth2_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `access_tokens` varchar(255) NOT NULL,
  `expire_time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `objectregister`
--

CREATE TABLE `objectregister` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unique_object_type_id` bigint(20) UNSIGNED NOT NULL,
  `statusdef_id` bigint(20) UNSIGNED DEFAULT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `object_unique_id` varchar(190) NOT NULL DEFAULT '00000000-0000-0000-00000000000000000',
  `modification_no` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `objectregister`
--
DELIMITER $$
CREATE TRIGGER `objectregister_BEFORE_INSERT` BEFORE INSERT ON `objectregister` FOR EACH ROW BEGIN
    DECLARE jetzt DATETIME;
    SET jetzt=NOW();
    set new.created_at = jetzt;
    set new.updated_at = jetzt;
    if( new.object_unique_id IS NULL or  new.object_unique_id = '' ) then
        set new.object_unique_id = UUID();
    end if;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `objectregister_BEFORE_UPDATE` BEFORE UPDATE ON `objectregister` FOR EACH ROW BEGIN
    set new.updated_at = NOW();
    if( new.object_unique_id <> old.object_unique_id ) then
        set new.object_unique_id = old.object_unique_id;
    end if;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `object_register_meta`
--

CREATE TABLE `object_register_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `object_register_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(190) NOT NULL,
  `meta_value` mediumtext,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `object_register_meta`
--
DELIMITER $$
CREATE TRIGGER `object_register_meta_BEFORE_INSERT` BEFORE INSERT ON `object_register_meta` FOR EACH ROW BEGIN
DECLARE jetzt DATETIME; SET jetzt=NOW();
set new.created_at = jetzt;
set new.updated_at = jetzt;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `object_register_meta_BEFORE_UPDATE_1` BEFORE UPDATE ON `object_register_meta` FOR EACH ROW BEGIN
set new.updated_at = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `object_type_meta`
--

CREATE TABLE `object_type_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unique_object_type_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(190) NOT NULL,
  `meta_value` mediumtext,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `object_type_meta`
--

INSERT INTO `object_type_meta` (`id`, `unique_object_type_id`, `meta_key`, `meta_value`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 'type_description', '{\"ProjectWizard\":{\"relation\":{\"company_name\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^[\\\\w,\\\\. -]*$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":null},\"company_id\":{\"phpdatatype\":\"int\",\"pattern\":\"\\/^[\\\\d]*$\\/\",\"filterValidate\":257,\"filterSanitize\":519,\"defaultvalue\":null},\"project_object_unique_id\":{\"phpdatatype\":\"int\",\"pattern\":\"\\/^[\\\\d]*$\\/\",\"filterValidate\":257,\"filterSanitize\":519,\"defaultvalue\":null},\"project_objectregister_id\":{\"phpdatatype\":\"int\",\"pattern\":\"\\/^[\\\\d]*$\\/\",\"filterValidate\":257,\"filterSanitize\":519,\"defaultvalue\":null}},\"settings\":{\"description\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^[\\\\w,\\\\. -]*$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":null},\"url_option\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^((folder)|(sub-domain)|(custom-domain)|(delegate-domain)){1}$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":\"folder\"},\"beginDate\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^(2[0-9]{3})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01]) (0[0-9]|1[0-9]|2[0123])\\\\:([012345][0-9])\\\\:([012345][0-9])$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":null},\"expireDate\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^(2[0-9]{3})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01]) (0[0-9]|1[0-9]|2[0123])\\\\:([012345][0-9])\\\\:([012345][0-9])$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":null}},\"advancedsettings\":{\"commercial_relationships\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^((b2b)|(b2c)|(both)){1}$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":\"both\"},\"register_multiple_times\":{\"phpdatatype\":\"int\",\"pattern\":\"\\/^[0-1]{1}$\\/\",\"filterValidate\":257,\"filterSanitize\":519,\"defaultvalue\":true},\"country\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^[a-z]*$\\/i\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":\"Germany\"},\"fraud_prevention_level\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^((low)|(medium)|(high)){1}$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":\"low\"},\"forward_lead_by_email\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^(?!(?:(?:\\\\x22?\\\\x5C[\\\\x00-\\\\x7E]\\\\x22?)|(?:\\\\x22?[^\\\\x5C\\\\x22]\\\\x22?)){255,})(?!(?:(?:\\\\x22?\\\\x5C[\\\\x00-\\\\x7E]\\\\x22?)|(?:\\\\x22?[^\\\\x5C\\\\x22]\\\\x22?)){65,}@)(?:(?:[\\\\x21\\\\x23-\\\\x27\\\\x2A\\\\x2B\\\\x2D\\\\x2F-\\\\x39\\\\x3D\\\\x3F\\\\x5E-\\\\x7E\\u00e4\\u00f6\\u00fc\\u00df]+)|(?:\\\\x22(?:[\\\\x01-\\\\x08\\\\x0B\\\\x0C\\\\x0E-\\\\x1F\\\\x21\\\\x23-\\\\x5B\\\\x5D-\\\\x7F\\u00e4\\u00f6\\u00fc\\u00df]|(?:\\\\x5C[\\\\x00-\\\\x7F]))*\\\\x22))(?:\\\\.(?:(?:[\\\\x21\\\\x23-\\\\x27\\\\x2A\\\\x2B\\\\x2D\\\\x2F-\\\\x39\\\\x3D\\\\x3F\\\\x5E-\\\\x7E\\u00e4\\u00f6\\u00fc\\u00df]+)|(?:\\\\x22(?:[\\\\x01-\\\\x08\\\\x0B\\\\x0C\\\\x0E-\\\\x1F\\\\x21\\\\x23-\\\\x5B\\\\x5D-\\\\x7F\\u00e4\\u00f6\\u00fc\\u00df]|(?:\\\\x5C[\\\\x00-\\\\x7F]))*\\\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9\\u00e4\\u00f6\\u00fc\\u00df]+(?:-[a-z0-9\\u00e4\\u00f6\\u00fc\\u00df]+)*\\\\.){1,126}){1,}(?:(?:[a-z\\u00e4\\u00f6\\u00fc\\u00df][a-z0-9\\u00e4\\u00f6\\u00fc\\u00df]*)|(?:(?:xn--)[a-z0-9\\u00e4\\u00f6\\u00fc\\u00df]+))(?:-[a-z0-9\\u00e4\\u00f6\\u00fc\\u00df]+)*)|(?:\\\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\\\]))$\\/iD\",\"filterValidate\":null,\"filterSanitize\":null,\"defaultvalue\":null},\"bool_redirect_after_registration\":{\"phpdatatype\":\"int\",\"pattern\":\"\\/^[0-1]{1}$\\/\",\"filterValidate\":257,\"filterSanitize\":519,\"defaultvalue\":false},\"redirect_after_registration\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^(?:(?:(?:https?):)?\\\\\\/\\\\\\/)(?:\\\\S+(?::\\\\S*)?@)?(?:(?!(?:10|127)(?:\\\\.\\\\d{1,3}){3})(?!(?:169\\\\.254|192\\\\.168)(?:\\\\.\\\\d{1,3}){2})(?!172\\\\.(?:1[6-9]|2\\\\d|3[0-1])(?:\\\\.\\\\d{1,3}){2})(?:[1-9]\\\\d?|1\\\\d\\\\d|2[01]\\\\d|22[0-3])(?:\\\\.(?:1?\\\\d{1,2}|2[0-4]\\\\d|25[0-5])){2}(?:\\\\.(?:[1-9]\\\\d?|1\\\\d\\\\d|2[0-4]\\\\d|25[0-4]))|(?:(?:[a-z\\\\u00a1-\\\\uffff0-9]-*)*[a-z\\\\u00a1-\\\\uffff0-9]+)(?:\\\\.(?:[a-z\\\\u00a1-\\\\uffff0-9]-*)*[a-z\\\\u00a1-\\\\uffff0-9]+)*(?:\\\\.(?:[a-z\\\\u00a1-\\\\uffff]{2,})))(?::\\\\d{2,5})?(?:[\\/?#]\\\\S*)?$\\/\",\"filterValidate\":273,\"filterSanitize\":518,\"defaultvalue\":null}},\"customfieldlist\":{\"phpdatatype\":\"array\",\"pattern\":null,\"filterValidate\":null,\"filterSanitize\":null,\"defaultvalue\":null},\"tracker\":{\"goolge_apikey\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^[\\\\w,\\\\. -]*$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":null},\"facebook_pixel\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^[\\\\w,\\\\. -]*$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":null},\"webgains_id\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^[\\\\w,\\\\. -]*$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":null},\"outbrain_id\":{\"phpdatatype\":\"string\",\"pattern\":\"\\/^[\\\\w,\\\\. -]*$\\/\",\"filterValidate\":null,\"filterSanitize\":513,\"defaultvalue\":null},\"bool_outbrain_track_after_30_seconds\":{\"phpdatatype\":\"int\",\"pattern\":\"\\/^[0-1]{1}$\\/\",\"filterValidate\":257,\"filterSanitize\":519,\"defaultvalue\":null}}}}', '2020-11-09 15:55:15', NULL, '2020-11-11 14:29:59', NULL),
(2, 1, 'type_default_settings', '{\"relation\":{\"company_name\":\"\",\"company_id\":\"\",\"project_object_unique_id\":\"\",\"project_objectregister_id\":\"\"},\"settings\":{\"description\":\"\",\"url_option\":\"\",\"beginDate\":\"\",\"expireDate\":\"\",\"url_options\":\"folder\"},\"advancedsettings\":{\"commercial_relationships\":\"both\",\"register_multiple_times\":true,\"country\":\"Germany\",\"fraud_prevention_level\":\"low\",\"forward_lead_by_email\":\"\",\"bool_redirect_after_registration\":false,\"redirect_after_registration\":\"\"},\"customfieldlist\":\"\",\"tracker\":{\"goolge_apikey\":\"\",\"facebook_pixel\":\"\",\"webgains_id\":\"\",\"outbrain_id\":\"\",\"bool_outbrain_track_after_30_seconds\":\"\"}}', '2020-11-09 15:55:42', NULL, '2020-11-11 19:37:32', NULL),
(3, 2, 'type_description', '{}', '2020-11-09 15:56:26', NULL, '2020-11-09 15:56:26', NULL),
(4, 2, 'type_default_settings', '{}', '2020-11-09 15:56:35', NULL, '2020-11-09 15:56:35', NULL),
(5, 3, 'type_description', '{}', '2020-11-09 15:56:48', NULL, '2020-11-09 15:56:48', NULL),
(6, 3, 'type_default_settings', '{}', '2020-11-09 15:56:57', NULL, '2020-11-09 15:56:57', NULL),
(7, 10, 'type_description', '{\"overview\":{\"title\":\"Email Server\",\"subTitle\":\"Sends Lead Notification and System Messages\",\"type\":[\"email\",\"all\"],\"releaseDate\":\"26.05.2020\",\"lastUpdate\":\"26.05.2020\",\"version\":\"1.0\"},\"relation\":{\"company_id\":\"\",\"integration_object_unique_id\":\"\",\"integration_objectregister_id\":\"\",\"integration_name\":\"EmailServer\",\"integration_type\":[\"email\",\"all\"]},\"settings\":{\"server_type\":[\"custom_server\",\"default_server\"],\"default_server\":{\"smtp_server_domain\":\"default.com\",\"sender_user_email\":\"test@default.com\",\"sender_password\":\"default\",\"port\":\"21\"},\"custom_server\":{\"smtp_server_domain\":\"\",\"sender_user_email\":\"\",\"sender_password\":\"\",\"port\":\"\"}}}', '2020-12-19 12:41:15', 1, '2021-02-28 22:08:13', 1),
(8, 10, 'type_default_settings', '{\"overview\":{\"title\":\"Email Server\",\"subTitle\":\"Sends Lead Notification and System Messages\",\"type\":[\"email\",\"all\"],\"releaseDate\":\"26.05.2020\",\"lastUpdate\":\"26.05.2020\",\"version\":\"1.0\"},\"relation\":{\"company_id\":\"\",\"integration_object_unique_id\":\"\",\"integration_objectregister_id\":\"\",\"integration_name\":\"EmailServer\",\"integration_type\":[\"email\",\"all\"]},\"settings\":{\"server_type\":[\"custom_server\",\"default_server\"],\"default_server\":{\"smtp_server_domain\":\"default.com\",\"sender_user_email\":\"test@default.com\",\"sender_password\":\"default\",\"port\":\"21\"},\"custom_server\":{\"smtp_server_domain\":\"\",\"sender_user_email\":\"\",\"sender_password\":\"\",\"port\":\"\"}}}', '2020-12-19 12:41:15', 1, '2021-02-28 22:08:55', 1),
(9, 11, 'type_description', '{\"overview\":{\"title\":\"eMail-In-One\",\"subTitle\":\"Newsletter,DOI,Trigger,Automation\",\"type\":[\"email\",\"all\"],\"releaseDate\":\"26.05.2020\",\"lastUpdate\":\"26.05.2020\",\"version\":\"1.0\"},\"relation\":{\"company_id\":\"\",\"integration_object_unique_id\":\"\",\"integration_objectregister_id\":\"\",\"integration_name\":\"eMail-In-One\",\"integration_type\":[\"email\",\"all\"]},\"settings\":{\"apikey\":\"\",\"blacklisting_id\":\"\",\"blacklisting_flagged_name\":\"\",\"reactivate_unsubscibers\":\"\",\"mapping\":\"\"}}', '2020-12-19 12:41:15', 1, '2020-12-19 12:41:15', 1),
(10, 11, 'type_default_settings', '{\"overview\":{\"title\":\"eMail-In-One\",\"subTitle\":\"Newsletter,DOI,Trigger,Automation\",\"type\":[\"email\",\"all\"],\"releaseDate\":\"26.05.2020\",\"lastUpdate\":\"26.05.2020\",\"version\":\"1.0\"},\"relation\":{\"company_id\":\"\",\"integration_object_unique_id\":\"\",\"integration_objectregister_id\":\"\",\"integration_name\":\"eMail-In-One\",\"integration_type\":[\"email\",\"all\"]},\"settings\":{\"apikey\":\"\",\"blacklisting_id\":\"\",\"blacklisting_flagged_name\":\"\",\"reactivate_unsubscibers\":\"\",\"mapping\":\"\"}}', '2020-12-19 12:41:15', 1, '2020-12-19 12:41:15', 1),
(11, 14, 'type_description', '{\"overview\":{\"title\":\"Hubspot\",\"subTitle\":\"Integrate to Hubspot CRM process\",\"type\":[\"crm\",\"all\"],\"releaseDate\":\"26.05.2020\",\"lastUpdate\":\"26.05.2020\",\"version\":\"1.0\"},\"relation\":{\"company_id\":\"\",\"integration_object_unique_id\":\"\",\"integration_objectregister_id\":\"\",\"integration_name\":\"digistore\",\"integration_type\":[\"online\",\"all\"]},\"settings\":{\"apikey\":\"\"}}', '2020-12-19 12:43:25', 1, '2021-07-14 11:25:12', 1),
(12, 14, 'type_default_settings', '{\"overview\":{\"title\":\"Hubspot\",\"subTitle\":\"Integrate to Hubspot CRM process\",\"type\":[\"crm\",\"all\"],\"releaseDate\":\"26.05.2020\",\"lastUpdate\":\"26.05.2020\",\"version\":\"1.0\"},\"relation\":{\"company_id\":\"\",\"integration_object_unique_id\":\"\",\"integration_objectregister_id\":\"\",\"integration_name\":\"digistore\",\"integration_type\":[\"online\",\"all\"]},\"settings\":{\"apikey\":\"\"}}', '2020-12-19 12:43:25', 1, '2021-07-14 11:25:18', 1),
(13, 12, 'type_description', '{\"overview\":{\"title\":\"Digistore\",\"subTitle\":\"Integration to marketplace for digital products\",\"type\":[\"online\",\"all\"],\"releaseDate\":\"26.05.2020\",\"lastUpdate\":\"26.05.2020\",\"version\":\"1.0\"},\"relation\":{\"company_id\":\"\",\"integration_object_unique_id\":\"\",\"integration_objectregister_id\":\"\",\"integration_name\":\"digistore\",\"integration_type\":[\"online\",\"all\"]},\"settings\":{\"apikey\":\"\"}}', '2020-12-19 12:43:25', 1, '2021-07-14 11:25:27', 1),
(14, 12, 'type_default_settings', '{\"overview\":{\"title\":\"Digistore\",\"subTitle\":\"Integration to marketplace for digital products\",\"type\":[\"online\",\"all\"],\"releaseDate\":\"26.05.2020\",\"lastUpdate\":\"26.05.2020\",\"version\":\"1.0\"},\"relation\":{\"company_id\":\"\",\"integration_object_unique_id\":\"\",\"integration_objectregister_id\":\"\",\"integration_name\":\"digistore\",\"integration_type\":[\"online\",\"all\"]},\"settings\":{\"apikey\":\"\"}}', '2020-12-19 12:43:25', 1, '2021-07-14 11:25:31', 1),
(15, 13, 'type_description', '{\"overview\":{\"title\":\"Webinaris\",\"subTitle\":\"Register leads for Webinars\",\"type\":[\"online\",\"all\"],\"releaseDate\":\"26.05.2020\",\"lastUpdate\":\"26.05.2020\",\"version\":\"1.0\"},\"relation\":{\"company_id\":\"\",\"integration_object_unique_id\":\"\",\"integration_objectregister_id\":\"\",\"integration_name\":\"webinaris\",\"integration_type\":[\"online\",\"all\"]},\"settings\":{\"apikey\":\"\"}}', '2020-12-19 12:43:25', 1, '2021-07-14 11:25:39', 1),
(16, 13, 'type_default_settings', '{\"overview\":{\"title\":\"Webinaris\",\"subTitle\":\"Register leads for Webinars\",\"type\":[\"online\",\"all\"],\"releaseDate\":\"26.05.2020\",\"lastUpdate\":\"26.05.2020\",\"version\":\"1.0\"},\"relation\":{\"company_id\":\"\",\"integration_object_unique_id\":\"\",\"integration_objectregister_id\":\"\",\"integration_name\":\"webinaris\",\"integration_type\":[\"online\",\"all\"]},\"settings\":{\"apikey\":\"\"}}', '2020-12-19 12:43:25', 1, '2021-07-14 11:25:43', 1),
(17, 18, 'type_default_settings', '{\"overview\":{\"name\":\"Send lead Notification\",\"acts\":\"serverside\",\"description\":\"sends lead notificaion\"},\"relation\":{\"company_id\":\"\",\"action_id\":1,\"input_datainterface_id\":\"\",\"output_datainterface_id\":\"\"},\"settings\":{\"name\":{\"type\":\"string\",\"validation\":\"^[a-zA-Z 0-9_.äöüß]*$\",\"value\":\"\"},\"list_of_recipients\":{\"type\":\"string\",\"validation\":\"^[a-zA-Z 0-9_.äöüß]*$\",\"value\":\"\"},\"subject_line\":{\"type\":\"string\",\"validation\":\"^[a-zA-Z 0-9_.äöüß]*$\",\"value\":\"\"}}}', '2021-04-12 16:56:58', 1, '2021-06-08 23:21:44', 1),
(18, 19, 'type_default_settings', '{\"overview\":{\"name\":\"Start Marketing Automation\",\"acts\":\"serverside\",\"description\":\"starts marketing automation\"},\"relation\":{\"company_id\":\"\",\"action_id\":2,\"input_datainterface_id\":\"\",\"output_datainterface_id\":\"\"},\"settings\":{\"name\":{\"type\":\"string\",\"validation\":\"^[a-zA-Z 0-9_.äöüß]*$\",\"value\":\"\"},\"marketing_automation_number\":{\"type\":\"int\",\"validation\":\"^[0-9]*$\",\"value\":\"\"}}}', '2021-04-12 16:56:58', 1, '2021-06-08 23:20:21', 1),
(19, 20, 'type_default_settings', '{\"overview\":{\"name\":\"Send Double Opt-In Email\",\"acts\":\"serverside\",\"description\":\"Send your lead an Email through eMail-In-One to confirm their subscription\"},\"relation\":{\"company_id\":\"\",\"action_id\":3,\"input_datainterface_id\":\"\",\"output_datainterface_id\":\"\"},\"settings\":{\"name\":{\"type\":\"string\",\"validation\":\"^[a-zA-Z 0-9_.]*$\",\"value\":\"\"},\"DOI_mailing\":{\"type\":\"string\",\"validation\":\"^[a-zA-Z 0-9_.]*$\",\"value\":\"\"},\"type_of_DOI\":{\"type\":\"string\",\"validation\":\"^[a-zA-Z 0-9_.]*$\",\"value\":\"\"}}}', '2021-04-18 13:01:38', 1, '2021-05-12 15:46:57', 1),
(20, 21, 'type_default_settings', '{\"overview\":{\"name\":\"Contact Event\",\"acts\":\"serverside\",\"description\":\"runs eMail-In-One transaction contact event\"},\"relation\":{\"project_campaign_id\":\"\",\"action_id\":4,\"input_datainterface_id\":\"\",\"output_datainterface_id\":\"\"},\"settings\":{\"processhookname\":\"\",\"contact_event_id\":\"\",\"contact_event_name\":\"\",\"field_mappings\":[{\"ce_field_id\":\"\",\"ce_field_name\":\"\",\"mio_field_id\":\"\",\"mio_field_name\":\"\",\"mio_sourcetable\":\"\"}]}}', '2021-04-19 10:28:01', 1, '2021-05-04 09:25:11', 1),
(21, 22, 'type_default_settings', '{\"overview\":{\"name\":\"Send Double Opt In Mail Or Call Contact Event\",\"acts\":\"serverside\",\"description\":\"runs eMail-In-One transaction contact event or sends DOI mail\"},\"relation\":{\"project_campaign_id\":\"\",\"action_id\":5,\"input_datainterface_id\":\"\",\"output_datainterface_id\":\"\"},\"settings\":{\"name\":\"\",\"doi_mailing_key\":\"\",\"doi_mailing_name\":\"\",\"contact_event_id\":\"\",\"contact_event_name\":\"\",\"field_mappings\":[{\"ce_field_id\":\"\",\"ce_field_name\":\"\",\"mio_field_id\":\"\",\"mio_field_name\":\"\",\"mio_sourcetable\":\"\"}]}}', '2021-05-12 03:53:21', 1, '2021-05-12 03:53:21', 1);

--
-- Trigger `object_type_meta`
--
DELIMITER $$
CREATE TRIGGER `object_type_meta_BEFORE_INSERT` BEFORE INSERT ON `object_type_meta` FOR EACH ROW BEGIN
DECLARE jetzt DATETIME;
SET jetzt=NOW();
set new.created_at = jetzt;
set new.updated_at = jetzt;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `object_type_meta_BEFORE_UPDATE` BEFORE UPDATE ON `object_type_meta` FOR EACH ROW BEGIN
set new.updated_at = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `output_datainterface`
--

CREATE TABLE `output_datainterface` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `data_source` varchar(45) DEFAULT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `processhook`
--

CREATE TABLE `processhook` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `input_datainterface_id` bigint(20) UNSIGNED DEFAULT NULL,
  `output_datainterface_id` bigint(20) UNSIGNED DEFAULT NULL,
  `switch_definition_id` bigint(20) UNSIGNED DEFAULT NULL,
  `acts` enum('serverside','clientside') DEFAULT 'serverside' COMMENT 'possible values serverside or clientsside',
  `name` varchar(255) NOT NULL,
  `description` text,
  `is_triggered_by_event_id` bigint(20) UNSIGNED DEFAULT NULL,
  `execution_status` varchar(45) DEFAULT NULL,
  `on_error_instruction` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `processhook`
--

INSERT INTO `processhook` (`id`, `action_id`, `input_datainterface_id`, `output_datainterface_id`, `switch_definition_id`, `acts`, `name`, `description`, `is_triggered_by_event_id`, `execution_status`, `on_error_instruction`) VALUES
(1, 1, NULL, NULL, NULL, 'serverside', 'send_lead_notification', 'Forward information from your Leads to one or more Email Addresses', NULL, NULL, NULL),
(2, 2, NULL, NULL, NULL, 'serverside', 'start_marketing_automation', 'Trigger a Marketing Automation in eMail-In-One or other platforms', NULL, NULL, NULL),
(3, 3, NULL, NULL, NULL, 'serverside', 'send_double_opt_in_mail', 'Send your lead an Email through eMail-In-One to confirm their subscription', NULL, NULL, NULL),
(4, 4, NULL, NULL, NULL, 'serverside', 'call_contact_event', 'Call a Contact Event through eMail-In-One to trigger Email events', NULL, NULL, NULL),
(5, 5, NULL, NULL, NULL, 'serverside', 'send_double_opt_in_mail_or_call_contact_event', 'A combination of both DOI Email and calling a Contact Event through eMail-In-One', NULL, NULL, NULL),
(6, 6, NULL, NULL, NULL, 'serverside', 'set_smart_tag', 'Define the names of Smart Tags used on your Page', NULL, NULL, NULL),
(7, 7, NULL, NULL, NULL, 'serverside', 'remove_smart_tag', 'Remove Smart Tags defined on your Page', NULL, NULL, NULL),
(8, 8, NULL, NULL, NULL, 'clientside', 'show_popup', 'Configure and show a Popup on your Page triggered by an Event', NULL, NULL, NULL),
(9, 8, NULL, NULL, NULL, 'clientside', 'hide_or_unhide_section', 'Hide Content on your Page based on target group or traffic category', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `survey_id` bigint(20) UNSIGNED NOT NULL,
  `context_id` bigint(20) UNSIGNED NOT NULL,
  `question_types_id` bigint(20) UNSIGNED NOT NULL,
  `question_question_txt` text NOT NULL COMMENT 'the survey question',
  `question_hint_txt` text COMMENT 'frontend: text hint for  leads about the question and how to answer it ...',
  `question_json_representation` mediumtext COMMENT 'MEDIUMTEXT must be changed to JSON datatype in modern MySql Version',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'decoupled users.id',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'decoupled users.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='ALTERED. VERSION 1.1 QUESTION TABLE BY JSR';

--
-- Trigger `questions`
--
DELIMITER $$
CREATE TRIGGER `questions_BEFORE_INSERT` BEFORE INSERT ON `questions` FOR EACH ROW BEGIN
set new.created_at = now();
set new.updated_at = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `questions_BEFORE_UPDATE` BEFORE UPDATE ON `questions` FOR EACH ROW BEGIN
set new.updated_at = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `question_context`
--

CREATE TABLE `question_context` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `context_category` varchar(60) NOT NULL COMMENT 'knowledge check:  possible  results like  yes or no ',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'decoupled users.id',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'users.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='NEW. VERSION 1.1 question_context TABLE BY JSR.';

--
-- Trigger `question_context`
--
DELIMITER $$
CREATE TRIGGER `context_BEFORE_UPDATE` BEFORE UPDATE ON `question_context` FOR EACH ROW BEGIN
	if( new.id <> old.id ) then
		signal sqlstate '45000' set message_text=" A QUESTION CONTEXT ID CAN NEVER BE CHANGED!";
	end if;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `question_context_BEFORE_INSERT` BEFORE INSERT ON `question_context` FOR EACH ROW BEGIN
set new.created_at = now();
set new.updated_at = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `question_types`
--

CREATE TABLE `question_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_type_html_tag` varchar(40) NOT NULL COMMENT 'inital values for html_tag:  select,  range, textarea, checkbox',
  `question_type_html_template` mediumtext COMMENT 'optional pre-defined html template for select,  checkboxes and so on',
  `question_type_frontend_hint` varchar(255) DEFAULT NULL COMMENT 'human synonmys for html tags to show in frontend\\\\nselect => Dropdown-list\\\\nrange => Slider\\\\ntextarea => Freitext-Antwort\\\\ncheckbox => Fragenkatalog',
  `question_type_description` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'decoupled users.id',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'decoupled users.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='NEW. VERSION 1.1 question_types TABLE BY JSR.';

--
-- Trigger `question_types`
--
DELIMITER $$
CREATE TRIGGER `question_types_BEFORE_DELETE` BEFORE DELETE ON `question_types` FOR EACH ROW BEGIN
signal sqlstate '45002' set message_text=" A QUESTION TYPE SHOULD NOT BE DELETED. BUT CAN BE DELETED AFTER DISABLING THIS TABLES BEFORE DELETE TRIGGER!";
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `question_types_BEFORE_INSERT` BEFORE INSERT ON `question_types` FOR EACH ROW BEGIN
set new.created_at = now();
set new.updated_at = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `question_types_BEFORE_UPDATE` BEFORE UPDATE ON `question_types` FOR EACH ROW BEGIN
set new.updated_at = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `securelog`
--

CREATE TABLE `securelog` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `company_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'delete that col since ref tab users has company_id',
  `logindate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(255) NOT NULL DEFAULT '',
  `result` enum('success','failure') NOT NULL DEFAULT 'success'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `securelog`
--
DELIMITER $$
CREATE TRIGGER `securelog_BEFORE_INSERT` BEFORE INSERT ON `securelog` FOR EACH ROW BEGIN
set new.logindate = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sessions`
--

CREATE TABLE `sessions` (
  `sess_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `sess_data` blob NOT NULL,
  `sess_time` int(10) UNSIGNED NOT NULL,
  `sess_lifetime` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `standardfieldmapping`
--

CREATE TABLE `standardfieldmapping` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `system_id` bigint(20) UNSIGNED NOT NULL,
  `miostandardfield_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `datatype` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `standardfieldmapping`
--

INSERT INTO `standardfieldmapping` (`id`, `system_id`, `miostandardfield_id`, `name`, `datatype`) VALUES
(1, 1, 1, 'EMAIL', 'string'),
(2, 1, 2, 'SALUTATION', 'string'),
(3, 1, 3, 'FIRSTNAME', 'string'),
(4, 1, 4, 'LASTNAME', 'string'),
(5, 1, 5, 'ORGANIZATION', 'string'),
(6, 1, 6, 'ADDRESS', 'string'),
(7, 1, 7, 'ZIP', 'string'),
(8, 1, 9, 'COUNTRY', 'string'),
(9, 1, 11, 'MIO_lead_reference', 'string'),
(10, 1, 14, 'MIO_trafficsource', 'string'),
(11, 1, 18, 'MIO_ip', 'string'),
(12, 1, 17, 'MIO_targetgroup', 'string'),
(35, 2, 1, 'email', 'string'),
(36, 2, 2, 'salutation', 'string'),
(37, 2, 3, 'firstname', 'string'),
(38, 2, 4, 'lastname', 'string'),
(39, 2, 5, 'company', 'string'),
(40, 2, 6, 'address', 'string'),
(41, 2, 7, 'zip', 'string'),
(42, 2, 8, 'city', 'string'),
(43, 2, 9, 'country', 'string'),
(44, 2, 10, 'phone', 'string'),
(45, 2, 11, 'MIO_lead_reference', 'string'),
(46, 2, 12, 'MIO_url', 'string'),
(47, 2, 14, 'MIO_trafficsource', 'string'),
(48, 2, 15, 'MIO_utm_parameters', 'string'),
(49, 2, 17, 'MIO_targetgroup', 'string'),
(50, 2, 18, 'MIO_ip', 'string'),
(51, 2, 19, 'MIO_device_type', 'string'),
(52, 2, 20, 'MIO_device_browser', 'string'),
(53, 2, 21, 'MIO_device_os', 'string'),
(54, 2, 22, 'MIO_device_os_version', 'string'),
(55, 2, 23, 'MIO_device_browser_version', 'string');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `statusdef`
--

CREATE TABLE `statusdef` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `value` varchar(80) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `statusdef`
--

INSERT INTO `statusdef` (`id`, `value`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'active', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(2, 'archived', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(3, 'draft', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(4, 'inactive', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(5, 'fraud', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(6, 'new', '2020-11-09 18:29:34', 1, '2020-11-09 18:29:34', 1),
(7, 'confirmed', '2020-11-09 18:29:52', 1, '2020-11-09 18:29:52', 1),
(8, 'markedfordeletion', '2020-11-09 18:31:02', 1, '2020-11-09 18:31:02', 1),
(9, 'unsubscribed', '2020-11-11 10:35:59', 1, '2020-11-11 10:35:59', 1),
(10, 'blacklisted', '2020-11-11 10:37:15', 1, '2020-11-11 10:37:15', 1),
(11, 'available', '2020-12-19 13:54:53', 1, '2020-12-19 13:54:53', 1),
(12, 'installed', '2020-12-19 13:54:53', 1, '2020-12-19 13:54:53', 1),
(13, 'blocked', '2021-02-18 12:33:41', 1, '2021-02-18 12:33:41', 1),
(14, 'defined', '2021-03-25 11:30:17', 1, '2021-03-25 11:30:17', 1);

--
-- Trigger `statusdef`
--
DELIMITER $$
CREATE TRIGGER `statusdef_BEFORE_INSERT` BEFORE INSERT ON `statusdef` FOR EACH ROW BEGIN
DECLARE jetzt DATETIME; SET jetzt=NOW();
set new.created_at = jetzt;
set new.updated_at = jetzt;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `statusdef_BEFORE_UPDATE` BEFORE UPDATE ON `statusdef` FOR EACH ROW BEGIN
set new.updated_at = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `status_model`
--

CREATE TABLE `status_model` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unique_object_type_id` bigint(20) UNSIGNED NOT NULL,
  `statusdef_id` bigint(20) UNSIGNED NOT NULL,
  `modelname` varchar(80) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `status_model`
--

INSERT INTO `status_model` (`id`, `unique_object_type_id`, `statusdef_id`, `modelname`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 1, 'project', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(2, 2, 1, 'page', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(3, 2, 2, 'page', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(4, 2, 3, 'page', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(5, 3, 1, 'webhook', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(6, 3, 2, 'webhook', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(7, 3, 3, 'webhook', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(8, 4, 1, 'customer', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(9, 4, 4, 'customer', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(10, 4, 5, 'customer', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(11, 7, 1, 'survey', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(12, 7, 2, 'survey', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(13, 7, 3, 'survey', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(14, 9, 6, 'account', '2020-11-09 18:32:28', 1, '2020-11-09 18:32:28', 1),
(15, 9, 7, 'account', '2020-11-09 18:32:44', 1, '2020-11-09 18:34:17', 1),
(16, 9, 1, 'account', '2020-11-09 18:34:32', 1, '2020-11-09 18:34:32', 1),
(17, 9, 8, 'account', '2020-11-09 18:34:52', 1, '2020-11-09 18:34:52', 1),
(18, 4, 9, 'customer', '2020-11-11 10:38:46', 1, '2020-11-11 10:38:46', 1),
(19, 4, 10, 'customer', '2020-11-11 10:39:02', 1, '2020-11-11 10:39:02', 1),
(20, 9, 13, 'account', '2021-03-01 10:40:41', 1, '2021-03-01 11:02:48', 1);

--
-- Trigger `status_model`
--
DELIMITER $$
CREATE TRIGGER `status_model_BEFORE_INSERT` BEFORE INSERT ON `status_model` FOR EACH ROW BEGIN
DECLARE jetzt DATETIME; SET jetzt=NOW();
set new.created_at = jetzt;
set new.updated_at = jetzt;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `status_model_BEFORE_UPDATE` BEFORE UPDATE ON `status_model` FOR EACH ROW BEGIN
set new.updated_at = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `survey`
--

CREATE TABLE `survey` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `objectregister_id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'could be further restricted by trigger since it can be only a campaign_id which has an objectregister_id of unique_object_type project, page or webhook',
  `layout_type_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'key from status table (Carousel or Single page)',
  `survey_is_online` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'flag to turn survey online (1)  or offline (0). default is on ( 1 )\\\\n',
  `survey_description` text COMMENT 'description of survey, add customer requirements or comments of the survey-maker here. add textarea to frontend.',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'decoupled users.id',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'decoupled users.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='NEW. VERSION 1.1 survey TABLE BY JSR.';

--
-- Trigger `survey`
--
DELIMITER $$
CREATE TRIGGER `survey_BEFORE_INSERT` BEFORE INSERT ON `survey` FOR EACH ROW BEGIN
set new.created_at = now();
set new.updated_at = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `survey_BEFORE_UPDATE` BEFORE UPDATE ON `survey` FOR EACH ROW BEGIN
set new.updated_at = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `switch_definition`
--

CREATE TABLE `switch_definition` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `switch_operators_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `expected_value` text,
  `check_value` text,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `switch_definition`
--

INSERT INTO `switch_definition` (`id`, `switch_operators_id`, `name`, `expected_value`, `check_value`, `description`) VALUES
(1, 5, 'email', '1', '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7Eäöüß]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7Fäöüß]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7Eäöüß]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7Fäöüß]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9äöüß]+(?:-[a-z0-9äöüß]+)*\\.){1,126}){1,}(?:(?:[a-zäöüß][a-z0-9äöüß]*)|(?:(?:xn--)[a-z0-9äöüß]+))(?:-[a-z0-9äöüß]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD', 'email must be a vaild email address. expected_value of the validation-check is 1 (true).');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `switch_operators`
--

CREATE TABLE `switch_operators` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `operator` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `switch_operators`
--

INSERT INTO `switch_operators` (`id`, `name`, `operator`) VALUES
(1, 'lr', '<'),
(2, 'gr', '>'),
(3, 'lreq', '<='),
(4, 'greq', '>='),
(5, 'eq', '==='),
(6, 'neq', '!=='),
(7, 'and', 'and'),
(8, 'or', 'or'),
(9, 'not', 'not'),
(10, 'xor', 'xor');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `system`
--

CREATE TABLE `system` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'hubspot, maileon, digistore, ...'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `system`
--

INSERT INTO `system` (`id`, `name`) VALUES
(1, 'email-in-one'),
(2, 'hubspot');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `unique_object_type`
--

CREATE TABLE `unique_object_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `assigned_table` varchar(80) NOT NULL COMMENT 'existing tablename of a table that imports objectregister.id as foreign key',
  `unique_object_name` varchar(190) NOT NULL COMMENT 'for example order-customfields, car-customfields, default-customfields',
  `has_versioning` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'object per dafault have versioning. currently ther is only project, which does not have versioning. if versioning is set to false, then the instance in objectregister will have only version = 1',
  `has_encryption` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `unique_object_type`
--

INSERT INTO `unique_object_type` (`id`, `assigned_table`, `unique_object_name`, `has_versioning`, `has_encryption`, `description`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'campaign', 'project', 0, 0, 'Project', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(2, 'campaign', 'page', 1, 0, 'LandingPage', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(3, 'campaign', 'webhook', 1, 0, 'Webhook', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(4, 'leads', 'customer', 1, 1, 'Customer', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(5, 'customfields', 'customfields', 0, 0, 'User defined customfields related to a project', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(6, 'customfields', 'standardcustomfields', 1, 0, 'User defined smarttags related to a project', '2020-10-23 19:19:11', 1, '2021-02-23 12:31:18', 1),
(7, 'survey', 'survey', 1, 0, 'Survey with answers related to a customer', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(8, 'survey', 'standalone survey', 1, 0, 'Survey with answers not related to any customer', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(9, 'company', 'account', 0, 0, 'Unique object type for user accounts in Marketing-In-One', '2020-11-09 16:00:57', 1, '2020-11-11 11:49:51', 1),
(10, 'integrations', 'emailserver', 0, 0, 'Email Server Integration for Project', '2020-12-19 12:37:10', 1, '2020-12-19 12:37:10', 1),
(11, 'integrations', 'email-in-one', 0, 0, 'eMail-In-One Integration for Project', '2020-12-19 12:37:26', 1, '2020-12-19 12:37:26', 1),
(12, 'integrations', 'digistore', 0, 0, 'Digistore Integration for Project', '2020-12-19 12:37:42', 1, '2020-12-19 12:37:42', 1),
(13, 'integrations', 'webinaris', 0, 0, 'Webinaris Integration for Project', '2020-12-19 12:37:54', 1, '2020-12-19 12:37:54', 1),
(14, 'integrations', 'hubspot', 0, 0, 'Hubspot Integration for Project', '2020-12-19 12:38:09', 1, '2020-12-19 12:38:09', 1),
(15, 'none', 'superadmin', 0, 0, 'Super Admin settings for Account', '2021-01-21 14:09:59', 1, '2021-01-21 14:09:59', 1),
(18, 'processhook', 'send_lead_notification', 0, 0, 'Forward information from your Leads to one or more Email Addresses', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(19, 'processhook', 'start_marketing_automation', 0, 0, 'Trigger a Marketing Automation in eMail-In-One or other platforms', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(20, 'processhook', 'send_double_opt_in_mail', 0, 0, 'Send your lead an Email through eMail-In-One to confirm their subscription', '2021-04-12 16:56:58', 1, '2021-04-18 12:55:10', 1),
(21, 'processhook', 'call_contact_event', 0, 0, 'Call a Contact Event through eMail-In-One to trigger Email events', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(22, 'processhook', 'send_double_opt_in_mail_or_call_contact_event', 0, 0, 'A combination of both DOI Email and calling a Contact Event through eMail-In-One', '2021-04-12 16:56:58', 1, '2021-04-21 13:36:58', 1),
(23, 'processhook', 'set_smart_tag', 0, 0, 'Define the names of Smart Tags used on your Page', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(24, 'processhook', 'remove_smart_tag', 0, 0, 'Remove Smart Tags defined on your Page', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(25, 'processhook', 'show_popup', 0, 0, 'Configure and show a Popup on your Page triggered by an Event', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(26, 'processhook', 'hide_or_unhide_section', 0, 0, 'Hide Content on your Page based on target group or traffic category', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(28, 'event', 'load_page', 0, 0, 'page load event', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(29, 'event', 'leave_page', 0, 0, 'page leave event', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(30, 'event', 'change_tab', 0, 0, 'tab change event', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(31, 'event', 'submit', 0, 0, 'submit click event', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(32, 'event', 'close_page', 0, 0, 'page close event', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(33, 'event', 'custom_event_button', 0, 0, 'custom buttons', '2021-04-12 16:56:58', 1, '2021-04-12 16:56:58', 1),
(38, 'workflow', 'workflow', 0, 0, 'Group of processhooks', '2021-04-20 17:32:07', 1, '2021-04-20 17:32:07', 1);

--
-- Trigger `unique_object_type`
--
DELIMITER $$
CREATE TRIGGER `unique_object_type_BEFORE_INSERT` BEFORE INSERT ON `unique_object_type` FOR EACH ROW BEGIN
DECLARE jetzt DATETIME; SET jetzt=NOW();
set new.created_at = jetzt;
set new.updated_at = jetzt;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `unique_object_type_BEFORE_UPDATE` BEFORE UPDATE ON `unique_object_type` FOR EACH ROW BEGIN
set new.updated_at = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED DEFAULT NULL,
  `email` varchar(180) NOT NULL,
  `password` varchar(255) NOT NULL,
  `roles` longtext,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `salutation` varchar(100) DEFAULT NULL,
  `registration` varchar(100) DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `failed_logins` int(11) NOT NULL DEFAULT '0',
  `super_admin` tinyint(1) NOT NULL DEFAULT '0',
  `permission` tinyint(1) NOT NULL DEFAULT '1',
  `dpa` tinyint(1) DEFAULT NULL,
  `dpa_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL,
  `status` enum('new','confirmed','active','blocked','inactive','blockedbyadmin') NOT NULL DEFAULT 'new'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `users`
--
DELIMITER $$
CREATE TRIGGER `users_BEFORE_INSERT` BEFORE INSERT ON `users` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `users_BEFORE_UPDATE` BEFORE UPDATE ON `users` FOR EACH ROW BEGIN
set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users_meta`
--

CREATE TABLE `users_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(190) DEFAULT NULL,
  `meta_value` longtext,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `users_meta`
--
DELIMITER $$
CREATE TRIGGER `users_meta_BEFORE_INSERT` BEFORE INSERT ON `users_meta` FOR EACH ROW BEGIN
DECLARE jetzt DATETIME;
SET jetzt=NOW();
set new.created = jetzt;
set new.updated = jetzt;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `users_meta_BEFORE_UPDATE` BEFORE UPDATE ON `users_meta` FOR EACH ROW BEGIN
set new.updated = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_processhook`
--

CREATE TABLE `user_processhook` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `processhook_id` bigint(20) UNSIGNED NOT NULL,
  `project_campaign_id` bigint(20) UNSIGNED NOT NULL,
  `objectregister_id` bigint(20) UNSIGNED NOT NULL,
  `processhook_name` varchar(255) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Trigger `user_processhook`
--
DELIMITER $$
CREATE TRIGGER `user_processhook_BEFORE_INSERT` BEFORE INSERT ON `user_processhook` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `user_processhook_BEFORE_UPDATE` BEFORE UPDATE ON `user_processhook` FOR EACH ROW BEGIN
set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_processhook_has_workflow`
--

CREATE TABLE `user_processhook_has_workflow` (
  `user_processhook_id` bigint(20) UNSIGNED NOT NULL,
  `workflow_id` bigint(20) UNSIGNED NOT NULL,
  `predecessor_uph_id` bigint(20) UNSIGNED DEFAULT NULL,
  `successor_uph_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `v_aio_get_lastest_lead_entry`
-- (Siehe unten für die tatsächliche Ansicht)
--
CREATE TABLE `v_aio_get_lastest_lead_entry` (
`id` bigint(20) unsigned
,`campaign_id` bigint(20) unsigned
,`objectregister_id` bigint(20) unsigned
,`account` bigint(20) unsigned
,`email` text
,`salutation` varchar(255)
,`first_name` text
,`last_name` text
,`firma` text
,`street` text
,`postal_code` text
,`city` text
,`country` text
,`phone` text
,`campaign` text
,`lead_reference` text
,`url` text
,`final_url` text
,`trafficsource` text
,`utm_parameters` text
,`split_version` text
,`targetgroup` text
,`affiliateID` text
,`affiliateSubID` text
,`ip` varchar(255)
,`device_type` varchar(255)
,`device_browser` varchar(255)
,`device_os` varchar(255)
,`device_os_version` varchar(255)
,`device_browser_version` varchar(255)
,`hash_email` varchar(128)
,`hash_salutation` varchar(128)
,`hash_first_name` varchar(128)
,`hash_last_name` varchar(128)
,`hash_firma` varchar(128)
,`hash_street` varchar(128)
,`hash_postal_code` varchar(128)
,`hash_city` varchar(128)
,`hash_country` varchar(128)
,`hash_phone` varchar(128)
,`hash_campaign` varchar(128)
,`hash_lead_reference` varchar(128)
,`hash_url` varchar(128)
,`hash_final_url` varchar(128)
,`hash_trafficsource` varchar(128)
,`hash_utm_parameters` varchar(128)
,`hash_split_version` varchar(128)
,`hash_targetgroup` varchar(128)
,`hash_affiliateID` varchar(128)
,`hash_affiliateSubID` varchar(128)
,`hash_ip` varchar(128)
,`hash_device_type` varchar(128)
,`hash_device_browser` varchar(128)
,`hash_device_os` varchar(128)
,`hash_device_os_version` varchar(128)
,`hash_device_browser_version` varchar(128)
,`modification` bigint(20) unsigned
,`created` datetime
,`created_by` int(11) unsigned
,`updated` datetime
,`updated_by` int(11) unsigned
,`version` int(10) unsigned
,`objreg_objectregister_id` bigint(20) unsigned
);

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `v_campaign_lead_infos`
-- (Siehe unten für die tatsächliche Ansicht)
--
CREATE TABLE `v_campaign_lead_infos` (
`CampaignId` bigint(20) unsigned
,`CompanyId` bigint(20) unsigned
,`Campaign Name` varchar(80)
,`lead_id` bigint(20) unsigned
,`Lead Reference` mediumtext
,`Created` datetime
,`Salutation` varchar(255)
,`Firstname` mediumtext
,`Lastname` mediumtext
,`Email` mediumtext
,`Postalcode` mediumtext
,`Trafficsource` mediumtext
,`UTM Parameters` mediumtext
,`AffiliateID` mediumtext
,`AffiliateSubID` mediumtext
,`name` mediumtext
,`value` longtext
);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `whitelist_domains`
--

CREATE TABLE `whitelist_domains` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `domain_name` varchar(255) DEFAULT NULL COMMENT 'White listed domain names',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `whitelist_domains`
--
DELIMITER $$
CREATE TRIGGER `whitelist_domains_BEFORE_INSERT` BEFORE INSERT ON `whitelist_domains` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `whitelist_domains_BEFORE_UPDATE` BEFORE UPDATE ON `whitelist_domains` FOR EACH ROW BEGIN
set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `workflow`
--

CREATE TABLE `workflow` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `objectregister_id` bigint(20) UNSIGNED NOT NULL,
  `page_webhook_campaign_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Trigger `workflow`
--
DELIMITER $$
CREATE TRIGGER `workflow_BEFORE_INSERT` BEFORE INSERT ON `workflow` FOR EACH ROW BEGIN
    DECLARE selected_unique_object_name VARCHAR(190);
    SET @new_campaign_id = new.page_webhook_campaign_id;

    SELECT lower(u.unique_object_name)
    INTO selected_unique_object_name
    FROM campaign c
    JOIN objectregister o on c.objectregister_id = o.id
    JOIN unique_object_type u ON o.unique_object_type_id = u.id
    WHERE c.id = @new_campaign_id;

    IF( selected_unique_object_name != 'page' AND selected_unique_object_name != 'webhook' )
    THEN
        signal sqlstate '45000' set message_text='campaign_id must be page or webhook';
    END IF;
    set new.created = now();
	set new.updated = now();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `workflow_BEFORE_UPDATE` BEFORE UPDATE ON `workflow` FOR EACH ROW BEGIN
    DECLARE selected_unique_object_name VARCHAR(190);
    SET @new_campaign_id = new.page_webhook_campaign_id;

    IF(  new.page_webhook_campaign_id != old.page_webhook_campaign_id)
    THEN

        SELECT lower(u.unique_object_name)
        INTO selected_unique_object_name
        FROM campaign c
        JOIN objectregister o on c.objectregister_id = o.id
        JOIN unique_object_type u ON o.unique_object_type_id = u.id
        WHERE c.id = @new_campaign_id;

        IF( selected_unique_object_name != 'page' AND selected_unique_object_name != 'webhook' )
        THEN
            signal sqlstate '45000' set message_text='campaign_id must be page or webhook';
        END IF;
    END IF;
    set new.updated = now();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `workflow_processhooks`
--

CREATE TABLE `workflow_processhooks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `workflow_id` bigint(20) UNSIGNED NOT NULL,
  `processhook_id` bigint(20) UNSIGNED NOT NULL,
  `switch_definition_id` bigint(20) UNSIGNED DEFAULT NULL,
  `predecessor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `successor_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur des Views `v_aio_get_lastest_lead_entry`
--
DROP TABLE IF EXISTS `v_aio_get_lastest_lead_entry`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mioadmin`@`%` SQL SECURITY DEFINER VIEW `v_aio_get_lastest_lead_entry`  AS SELECT `l`.`id` AS `id`, `l`.`campaign_id` AS `campaign_id`, `l`.`objectregister_id` AS `objectregister_id`, `l`.`account` AS `account`, `l`.`email` AS `email`, `l`.`salutation` AS `salutation`, `l`.`first_name` AS `first_name`, `l`.`last_name` AS `last_name`, `l`.`firma` AS `firma`, `l`.`street` AS `street`, `l`.`postal_code` AS `postal_code`, `l`.`city` AS `city`, `l`.`country` AS `country`, `l`.`phone` AS `phone`, `l`.`campaign` AS `campaign`, `l`.`lead_reference` AS `lead_reference`, `l`.`url` AS `url`, `l`.`final_url` AS `final_url`, `l`.`trafficsource` AS `trafficsource`, `l`.`utm_parameters` AS `utm_parameters`, `l`.`split_version` AS `split_version`, `l`.`targetgroup` AS `targetgroup`, `l`.`affiliateID` AS `affiliateID`, `l`.`affiliateSubID` AS `affiliateSubID`, `l`.`ip` AS `ip`, `l`.`device_type` AS `device_type`, `l`.`device_browser` AS `device_browser`, `l`.`device_os` AS `device_os`, `l`.`device_os_version` AS `device_os_version`, `l`.`device_browser_version` AS `device_browser_version`, `l`.`hash_email` AS `hash_email`, `l`.`hash_salutation` AS `hash_salutation`, `l`.`hash_first_name` AS `hash_first_name`, `l`.`hash_last_name` AS `hash_last_name`, `l`.`hash_firma` AS `hash_firma`, `l`.`hash_street` AS `hash_street`, `l`.`hash_postal_code` AS `hash_postal_code`, `l`.`hash_city` AS `hash_city`, `l`.`hash_country` AS `hash_country`, `l`.`hash_phone` AS `hash_phone`, `l`.`hash_campaign` AS `hash_campaign`, `l`.`hash_lead_reference` AS `hash_lead_reference`, `l`.`hash_url` AS `hash_url`, `l`.`hash_final_url` AS `hash_final_url`, `l`.`hash_trafficsource` AS `hash_trafficsource`, `l`.`hash_utm_parameters` AS `hash_utm_parameters`, `l`.`hash_split_version` AS `hash_split_version`, `l`.`hash_targetgroup` AS `hash_targetgroup`, `l`.`hash_affiliateID` AS `hash_affiliateID`, `l`.`hash_affiliateSubID` AS `hash_affiliateSubID`, `l`.`hash_ip` AS `hash_ip`, `l`.`hash_device_type` AS `hash_device_type`, `l`.`hash_device_browser` AS `hash_device_browser`, `l`.`hash_device_os` AS `hash_device_os`, `l`.`hash_device_os_version` AS `hash_device_os_version`, `l`.`hash_device_browser_version` AS `hash_device_browser_version`, `l`.`modification` AS `modification`, `l`.`created` AS `created`, `l`.`created_by` AS `created_by`, `l`.`updated` AS `updated`, `l`.`updated_by` AS `updated_by`, `o`.`version` AS `version`, `o`.`id` AS `objreg_objectregister_id` FROM (((`leads` `l` join `objectregister` `o` on((`o`.`id` = `l`.`objectregister_id`))) join `statusdef` `s` on(((`o`.`statusdef_id` = `s`.`id`) and (`s`.`value` = 'active')))) join `unique_object_type` `uot` on(((`o`.`unique_object_type_id` = `uot`.`id`) and (`uot`.`assigned_table` = 'leads')))) ;

-- --------------------------------------------------------

--
-- Struktur des Views `v_campaign_lead_infos`
--
DROP TABLE IF EXISTS `v_campaign_lead_infos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mioadmin`@`%` SQL SECURITY DEFINER VIEW `v_campaign_lead_infos`  AS SELECT `c`.`id` AS `CampaignId`, `c`.`company_id` AS `CompanyId`, `c`.`name` AS `Campaign Name`, `l`.`id` AS `lead_id`, `l`.`lead_reference` AS `Lead Reference`, `l`.`created` AS `Created`, `l`.`salutation` AS `Salutation`, `l`.`first_name` AS `Firstname`, `l`.`last_name` AS `Lastname`, `l`.`email` AS `Email`, `l`.`postal_code` AS `Postalcode`, `l`.`trafficsource` AS `Trafficsource`, `l`.`utm_parameters` AS `UTM Parameters`, `l`.`affiliateID` AS `AffiliateID`, `l`.`affiliateSubID` AS `AffiliateSubID`, `cf`.`fieldname` AS `name`, `lcf`.`lead_customfield_content` AS `value` FROM (((`campaign` `c` join `leads` `l` on((`l`.`campaign_id` = `c`.`id`))) join `customfields` `cf` on((`cf`.`campaign_id` = `c`.`id`))) join `lead_customfield_content` `lcf` on(((`lcf`.`leads_id` = `l`.`id`) and (`lcf`.`leads_id` = `l`.`id`)))) ;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `administrates_companies`
--
ALTER TABLE `administrates_companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_admin_comp_user_id_company_id` (`user_id`,`company_id`),
  ADD KEY `fk_admin_comp_company_id_idx` (`company_id`),
  ADD KEY `fk_admin_comp_user_id_idx` (`user_id`);

--
-- Indizes für die Tabelle `administrates_users`
--
ALTER TABLE `administrates_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_administrates_users` (`user_id`,`invited_user_id`,`to_company_id`),
  ADD KEY `fk_administrates_users_to_company_id_idx` (`to_company_id`),
  ADD KEY `fk_administrates_users_user_id_idx` (`user_id`),
  ADD KEY `fk_administrates_users_invited_user_id_idx` (`invited_user_id`);

--
-- Indizes für die Tabelle `blacklist_domains`
--
ALTER TABLE `blacklist_domains`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_campaign_name_version_status_compid` (`name`,`company_id`,`objectregister_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `idx_campaign_name` (`name`),
  ADD KEY `uk_campaign_account_no` (`account_no`),
  ADD KEY `fk_objectregister_idx` (`objectregister_id`);

--
-- Indizes für die Tabelle `clientside_pagetypes`
--
ALTER TABLE `clientside_pagetypes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_clientside_pagetypes_processhook1_idx` (`processhook_id`);

--
-- Indizes für die Tabelle `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uidx_account_no` (`account_no`),
  ADD KEY `id` (`id`),
  ADD KEY `fk_company_objectregister_id_idx` (`objectregister_id`);

--
-- Indizes für die Tabelle `customer_journey`
--
ALTER TABLE `customer_journey`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Index` (`lead_id`);

--
-- Indizes für die Tabelle `customfields`
--
ALTER TABLE `customfields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cf_campaign_idx` (`campaign_id`),
  ADD KEY `fk_customfields_datatypes1_idx` (`datatypes_id`),
  ADD KEY `fk_customfields_object_register1_idx` (`objectregister_id`);

--
-- Indizes für die Tabelle `datainterface_fields`
--
ALTER TABLE `datainterface_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_datainterface_fields_output_datainterface1_idx` (`output_datainterface_id`),
  ADD KEY `fk_datainterface_fields_input_datainterface1_idx` (`input_datainterface_id`),
  ADD KEY `fk_objectregister_id_idx` (`source_objectregister_id`);

--
-- Indizes für die Tabelle `datatypes`
--
ALTER TABLE `datatypes`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `fraud`
--
ALTER TABLE `fraud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idxfk_fraud_campaign_id` (`campaign_id`);

--
-- Indizes für die Tabelle `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_images_questions_idx` (`question_id`);

--
-- Indizes für die Tabelle `input_datainterface`
--
ALTER TABLE `input_datainterface`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `integrations`
--
ALTER TABLE `integrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_campaign_idx` (`campaign_id`),
  ADD KEY `fk_integrations_company_idx` (`company_id`),
  ADD KEY `fk_objectregister_idx` (`objectregister_id`);

--
-- Indizes für die Tabelle `layout_type`
--
ALTER TABLE `layout_type`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_id` (`campaign_id`),
  ADD KEY `account` (`account`),
  ADD KEY `fk_obj_reg_idx` (`objectregister_id`),
  ADD KEY `ix_hash_email` (`hash_email`),
  ADD KEY `ix_hash_first_name` (`hash_first_name`),
  ADD KEY `ix_hash_last_name` (`hash_last_name`),
  ADD KEY `ix_hash_firma` (`hash_firma`),
  ADD KEY `ix_hash_postal_code` (`hash_postal_code`),
  ADD KEY `ix_hash_city` (`hash_city`),
  ADD KEY `ix_hash_country` (`hash_country`),
  ADD KEY `ix_hash_campaign` (`hash_campaign`),
  ADD KEY `ix_hash_lead_reference` (`hash_lead_reference`),
  ADD KEY `ix_hash_salutation` (`hash_salutation`);

--
-- Indizes für die Tabelle `lead_answer_content`
--
ALTER TABLE `lead_answer_content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uidx_leads_questions` (`leads_id`,`questions_id`),
  ADD KEY `fk_lac_questions_idx` (`questions_id`);

--
-- Indizes für die Tabelle `lead_customfield_content`
--
ALTER TABLE `lead_customfield_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_leads_idx` (`leads_id`),
  ADD KEY `fk_customfields_idx` (`customfields_id`);

--
-- Indizes für die Tabelle `lead_sync_log`
--
ALTER TABLE `lead_sync_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lead_sync_log_integrations_id_fk` (`integration_id`);

--
-- Indizes für die Tabelle `miostandardfield`
--
ALTER TABLE `miostandardfield`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_standardfields_datatypes1_idx` (`datatypes_id`);

--
-- Indizes für die Tabelle `oauth2_tokens`
--
ALTER TABLE `oauth2_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indizes für die Tabelle `objectregister`
--
ALTER TABLE `objectregister`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_ouid_version` (`object_unique_id`,`version`),
  ADD KEY `fk_obj_reg_obj_type1_idx` (`unique_object_type_id`),
  ADD KEY `fk_obj_reg_statusdef1_idx` (`statusdef_id`);

--
-- Indizes für die Tabelle `object_register_meta`
--
ALTER TABLE `object_register_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_object_register_meta_object_register1_idx` (`object_register_id`);

--
-- Indizes für die Tabelle `object_type_meta`
--
ALTER TABLE `object_type_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_object_register_meta_unique_object_type1_idx` (`unique_object_type_id`);

--
-- Indizes für die Tabelle `output_datainterface`
--
ALTER TABLE `output_datainterface`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `processhook`
--
ALTER TABLE `processhook`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_processhook_action1_idx` (`action_id`),
  ADD KEY `fk_event_idx` (`is_triggered_by_event_id`),
  ADD KEY `fk_processhook_switch_definition1_idx` (`switch_definition_id`),
  ADD KEY `fk_processhook_input_datainterface1_idx` (`input_datainterface_id`),
  ADD KEY `fk_processhook_output_datainterface1_idx` (`output_datainterface_id`);

--
-- Indizes für die Tabelle `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_questions_survey1_idx` (`survey_id`),
  ADD KEY `fk_questions_context1_idx` (`context_id`),
  ADD KEY `fk_questions_question_types1_idx` (`question_types_id`);

--
-- Indizes für die Tabelle `question_context`
--
ALTER TABLE `question_context`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `question_types`
--
ALTER TABLE `question_types`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `securelog`
--
ALTER TABLE `securelog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indizes für die Tabelle `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sess_id`);

--
-- Indizes für die Tabelle `standardfieldmapping`
--
ALTER TABLE `standardfieldmapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_standardfieldmapping_system1_idx` (`system_id`),
  ADD KEY `fk_standardfieldmapping_miostandardfield1_idx` (`miostandardfield_id`);

--
-- Indizes für die Tabelle `statusdef`
--
ALTER TABLE `statusdef`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_value` (`value`);

--
-- Indizes für die Tabelle `status_model`
--
ALTER TABLE `status_model`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_object_type_id` (`unique_object_type_id`,`statusdef_id`),
  ADD KEY `fk_status_model_statusdef1_idx` (`statusdef_id`),
  ADD KEY `fk_unique_object_type_id_idx` (`unique_object_type_id`);

--
-- Indizes für die Tabelle `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_status1_idx` (`layout_type_id`),
  ADD KEY `fk_objectregister_sy_idx` (`objectregister_id`),
  ADD KEY `fk_campaign_id_idx` (`campaign_id`);

--
-- Indizes für die Tabelle `switch_definition`
--
ALTER TABLE `switch_definition`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_switch_definition_switch_operators1_idx` (`switch_operators_id`);

--
-- Indizes für die Tabelle `switch_operators`
--
ALTER TABLE `switch_operators`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `system`
--
ALTER TABLE `system`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `unique_object_type`
--
ALTER TABLE `unique_object_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_unique_object_name` (`unique_object_name`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_users_email` (`email`),
  ADD KEY `company_id` (`company_id`);

--
-- Indizes für die Tabelle `users_meta`
--
ALTER TABLE `users_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_idx` (`users_id`);

--
-- Indizes für die Tabelle `user_processhook`
--
ALTER TABLE `user_processhook`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_id_name` (`processhook_name`,`project_campaign_id`),
  ADD KEY `fk_user_processhook_processhook11_idx` (`processhook_id`),
  ADD KEY `fk_user_processhook_campaign11_idx` (`project_campaign_id`),
  ADD KEY `fk_objectregister11_idx` (`objectregister_id`);

--
-- Indizes für die Tabelle `user_processhook_has_workflow`
--
ALTER TABLE `user_processhook_has_workflow`
  ADD PRIMARY KEY (`user_processhook_id`,`workflow_id`),
  ADD KEY `fk_user_processhook_has_workflow_workflow1_idx` (`workflow_id`),
  ADD KEY `fk_user_processhook_has_workflow_user_processhook1_idx` (`user_processhook_id`),
  ADD KEY `fk_predessor_idx` (`predecessor_uph_id`),
  ADD KEY `fk_successor_idx` (`successor_uph_id`);

--
-- Indizes für die Tabelle `whitelist_domains`
--
ALTER TABLE `whitelist_domains`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `workflow`
--
ALTER TABLE `workflow`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_objectregister_workflow_idx` (`objectregister_id`),
  ADD KEY `fk_page_webhook_idx` (`page_webhook_campaign_id`);

--
-- Indizes für die Tabelle `workflow_processhooks`
--
ALTER TABLE `workflow_processhooks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_ph_wf` (`workflow_id`,`processhook_id`),
  ADD KEY `fk_processhook_has_workflow_workflow1_idx` (`workflow_id`),
  ADD KEY `fk_processhook_has_workflow_processhook1_idx` (`processhook_id`),
  ADD KEY `fk_processhook_has_workflow_processhook2_idx1` (`predecessor_id`),
  ADD KEY `fk_processhook_has_workflow_processhook3_idx2` (`successor_id`),
  ADD KEY `fk_workflow_processhooks_switch_definition1_idx` (`switch_definition_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `action`
--
ALTER TABLE `action`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT für Tabelle `administrates_companies`
--
ALTER TABLE `administrates_companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `administrates_users`
--
ALTER TABLE `administrates_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `blacklist_domains`
--
ALTER TABLE `blacklist_domains`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `campaign`
--
ALTER TABLE `campaign`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `clientside_pagetypes`
--
ALTER TABLE `clientside_pagetypes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `company`
--
ALTER TABLE `company`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `customer_journey`
--
ALTER TABLE `customer_journey`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `customfields`
--
ALTER TABLE `customfields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `datainterface_fields`
--
ALTER TABLE `datainterface_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `datatypes`
--
ALTER TABLE `datatypes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT für Tabelle `event`
--
ALTER TABLE `event`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `fraud`
--
ALTER TABLE `fraud`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `input_datainterface`
--
ALTER TABLE `input_datainterface`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `integrations`
--
ALTER TABLE `integrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `layout_type`
--
ALTER TABLE `layout_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `leads`
--
ALTER TABLE `leads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `lead_answer_content`
--
ALTER TABLE `lead_answer_content`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `lead_customfield_content`
--
ALTER TABLE `lead_customfield_content`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `lead_sync_log`
--
ALTER TABLE `lead_sync_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `miostandardfield`
--
ALTER TABLE `miostandardfield`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT für Tabelle `oauth2_tokens`
--
ALTER TABLE `oauth2_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `objectregister`
--
ALTER TABLE `objectregister`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `object_register_meta`
--
ALTER TABLE `object_register_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `object_type_meta`
--
ALTER TABLE `object_type_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT für Tabelle `output_datainterface`
--
ALTER TABLE `output_datainterface`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `processhook`
--
ALTER TABLE `processhook`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT für Tabelle `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `question_context`
--
ALTER TABLE `question_context`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `question_types`
--
ALTER TABLE `question_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `securelog`
--
ALTER TABLE `securelog`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `standardfieldmapping`
--
ALTER TABLE `standardfieldmapping`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT für Tabelle `statusdef`
--
ALTER TABLE `statusdef`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT für Tabelle `status_model`
--
ALTER TABLE `status_model`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT für Tabelle `survey`
--
ALTER TABLE `survey`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `switch_definition`
--
ALTER TABLE `switch_definition`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `switch_operators`
--
ALTER TABLE `switch_operators`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT für Tabelle `system`
--
ALTER TABLE `system`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `unique_object_type`
--
ALTER TABLE `unique_object_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `users_meta`
--
ALTER TABLE `users_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `user_processhook`
--
ALTER TABLE `user_processhook`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `whitelist_domains`
--
ALTER TABLE `whitelist_domains`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';

--
-- AUTO_INCREMENT für Tabelle `workflow`
--
ALTER TABLE `workflow`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `workflow_processhooks`
--
ALTER TABLE `workflow_processhooks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `administrates_companies`
--
ALTER TABLE `administrates_companies`
  ADD CONSTRAINT `fk_admin_comp_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_admin_comp_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `administrates_users`
--
ALTER TABLE `administrates_users`
  ADD CONSTRAINT `fk_administrates_users_invited_user_id` FOREIGN KEY (`invited_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_administrates_users_to_company_id` FOREIGN KEY (`to_company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_administrates_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `campaign`
--
ALTER TABLE `campaign`
  ADD CONSTRAINT `fk_company__id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_objectregister` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `clientside_pagetypes`
--
ALTER TABLE `clientside_pagetypes`
  ADD CONSTRAINT `fk_clientside_pagetypes_processhook1` FOREIGN KEY (`processhook_id`) REFERENCES `processhook` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `fk_company_objectregister` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE;

--
-- Constraints der Tabelle `customer_journey`
--
ALTER TABLE `customer_journey`
  ADD CONSTRAINT `cj_leads_ibfk_1` FOREIGN KEY (`lead_id`) REFERENCES `leads` (`id`) ON DELETE CASCADE;

--
-- Constraints der Tabelle `customfields`
--
ALTER TABLE `customfields`
  ADD CONSTRAINT `fk_cf_campaign` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_customfields_datatypes1` FOREIGN KEY (`datatypes_id`) REFERENCES `datatypes` (`id`),
  ADD CONSTRAINT `fk_customfields_object_register1` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE;

--
-- Constraints der Tabelle `datainterface_fields`
--
ALTER TABLE `datainterface_fields`
  ADD CONSTRAINT `fk_datainterface_fields_input_datainterface1` FOREIGN KEY (`input_datainterface_id`) REFERENCES `input_datainterface` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_datainterface_fields_output_datainterface1` FOREIGN KEY (`output_datainterface_id`) REFERENCES `output_datainterface` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_objectregister_id` FOREIGN KEY (`source_objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `fraud`
--
ALTER TABLE `fraud`
  ADD CONSTRAINT `fk_fraud_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `fk_images_questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `integrations`
--
ALTER TABLE `integrations`
  ADD CONSTRAINT `fk_integration_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_integrations_campaign` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_integrations_objectregister` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `leads`
--
ALTER TABLE `leads`
  ADD CONSTRAINT `fk_leads_campaign` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_leads_company` FOREIGN KEY (`account`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_object_register` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `lead_answer_content`
--
ALTER TABLE `lead_answer_content`
  ADD CONSTRAINT `fk_lac_leads` FOREIGN KEY (`leads_id`) REFERENCES `leads` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lac_questions` FOREIGN KEY (`questions_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints der Tabelle `lead_customfield_content`
--
ALTER TABLE `lead_customfield_content`
  ADD CONSTRAINT `lead_customfield_content_customfields_id_fk` FOREIGN KEY (`customfields_id`) REFERENCES `customfields` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lead_customfield_content_leads_id_fk` FOREIGN KEY (`leads_id`) REFERENCES `leads` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `lead_sync_log`
--
ALTER TABLE `lead_sync_log`
  ADD CONSTRAINT `lead_sync_log_integrations_id_fk` FOREIGN KEY (`integration_id`) REFERENCES `integrations` (`id`);

--
-- Constraints der Tabelle `miostandardfield`
--
ALTER TABLE `miostandardfield`
  ADD CONSTRAINT `fk_standardfields_datatypes1` FOREIGN KEY (`datatypes_id`) REFERENCES `datatypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `oauth2_tokens`
--
ALTER TABLE `oauth2_tokens`
  ADD CONSTRAINT `oauth2_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `objectregister`
--
ALTER TABLE `objectregister`
  ADD CONSTRAINT `fk_obj_reg_obj_type_1` FOREIGN KEY (`unique_object_type_id`) REFERENCES `unique_object_type` (`id`),
  ADD CONSTRAINT `fk_obj_reg_statusdef_1` FOREIGN KEY (`statusdef_id`) REFERENCES `statusdef` (`id`);

--
-- Constraints der Tabelle `object_register_meta`
--
ALTER TABLE `object_register_meta`
  ADD CONSTRAINT `fk_object_register_meta_object_register1` FOREIGN KEY (`object_register_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE;

--
-- Constraints der Tabelle `object_type_meta`
--
ALTER TABLE `object_type_meta`
  ADD CONSTRAINT `fk_object_register_meta_unique_object_type1` FOREIGN KEY (`unique_object_type_id`) REFERENCES `unique_object_type` (`id`);

--
-- Constraints der Tabelle `processhook`
--
ALTER TABLE `processhook`
  ADD CONSTRAINT `fk_event` FOREIGN KEY (`is_triggered_by_event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_processhook_action1` FOREIGN KEY (`action_id`) REFERENCES `action` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_processhook_input_datainterface1` FOREIGN KEY (`input_datainterface_id`) REFERENCES `input_datainterface` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_processhook_output_datainterface1` FOREIGN KEY (`output_datainterface_id`) REFERENCES `output_datainterface` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_processhook_switch_definition1` FOREIGN KEY (`switch_definition_id`) REFERENCES `switch_definition` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `fk_questions_context1` FOREIGN KEY (`context_id`) REFERENCES `question_context` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_questions_question_types1` FOREIGN KEY (`question_types_id`) REFERENCES `question_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_questions_survey1` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`) ON DELETE CASCADE;

--
-- Constraints der Tabelle `securelog`
--
ALTER TABLE `securelog`
  ADD CONSTRAINT `fk_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `standardfieldmapping`
--
ALTER TABLE `standardfieldmapping`
  ADD CONSTRAINT `fk_standardfieldmapping_miostandardfield1` FOREIGN KEY (`miostandardfield_id`) REFERENCES `miostandardfield` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_standardfieldmapping_system1` FOREIGN KEY (`system_id`) REFERENCES `system` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `status_model`
--
ALTER TABLE `status_model`
  ADD CONSTRAINT `fk_statusdef` FOREIGN KEY (`statusdef_id`) REFERENCES `statusdef` (`id`),
  ADD CONSTRAINT `fk_unique_object_type_id` FOREIGN KEY (`unique_object_type_id`) REFERENCES `unique_object_type` (`id`);

--
-- Constraints der Tabelle `survey`
--
ALTER TABLE `survey`
  ADD CONSTRAINT `fk_objectregister_sy` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_survey_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_survey_status` FOREIGN KEY (`layout_type_id`) REFERENCES `layout_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `switch_definition`
--
ALTER TABLE `switch_definition`
  ADD CONSTRAINT `fk_switch_definition_switch_operators1` FOREIGN KEY (`switch_operators_id`) REFERENCES `switch_operators` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `users_meta`
--
ALTER TABLE `users_meta`
  ADD CONSTRAINT `fk_users_users_meta` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `user_processhook`
--
ALTER TABLE `user_processhook`
  ADD CONSTRAINT `fk_objectregister11` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_processhook_processhook11` FOREIGN KEY (`processhook_id`) REFERENCES `processhook` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `user_processhook_has_workflow`
--
ALTER TABLE `user_processhook_has_workflow`
  ADD CONSTRAINT `fk_predessor` FOREIGN KEY (`predecessor_uph_id`) REFERENCES `user_processhook` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_successor` FOREIGN KEY (`successor_uph_id`) REFERENCES `user_processhook` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_processhook_has_workflow_user_processhook1` FOREIGN KEY (`user_processhook_id`) REFERENCES `user_processhook` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_processhook_has_workflow_workflow1` FOREIGN KEY (`workflow_id`) REFERENCES `workflow` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `workflow`
--
ALTER TABLE `workflow`
  ADD CONSTRAINT `fk_objectregister_workflow` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_page_webhook` FOREIGN KEY (`page_webhook_campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `workflow_processhooks`
--
ALTER TABLE `workflow_processhooks`
  ADD CONSTRAINT `fk_processhook_has_workflow_processhook1` FOREIGN KEY (`processhook_id`) REFERENCES `processhook` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_processhook_has_workflow_processhook2` FOREIGN KEY (`predecessor_id`) REFERENCES `processhook` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_processhook_has_workflow_processhook3` FOREIGN KEY (`successor_id`) REFERENCES `processhook` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_processhook_has_workflow_workflow1` FOREIGN KEY (`workflow_id`) REFERENCES `workflow` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_workflow_processhooks_switch_definition1` FOREIGN KEY (`switch_definition_id`) REFERENCES `switch_definition` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
