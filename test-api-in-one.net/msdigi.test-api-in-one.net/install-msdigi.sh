# install-msdigi.shcd
# install msdigi.test-api-in-one
# login as treaction-kto206-api
# ###############################################
#   configuration
# ###############################################
APPNAME="MSDIGI.TEST-API-IN-ONE"
USER1="treaction-kto206-api"
USER2=`whoami`
APPLICATION_PATH="/var/www/vhosts/test-api-in-one.net/msdigi.test-api-in-one.net"
CLONE_PATH="$APPLICATION_PATH/clone/msdigi"
# !!!!!!!!!! this reop must be cleaned up => ToolService.php is missing, vendor/digistore_api must be placed in scr/ExternalApis/digistore_api/ !!!!!!!
CLONE="https://Jochen_Treaction@bitbucket.org/pradeepveera1989/msdigi.git"
HTACCESSFORPUBLIC="/var/www/vhosts/test-api-in-one.net/htaccessForPublic"
ROBOTSTXT="/var/www/vhosts/test-api-in-one.net/robots.txt"
# ###############################################
if [ "$USER1" = "$USER2" ]; then
  echo ">>> INSTALLING $APPNAME ... "
  GOTODIR="$APPLICATION_PATH/clone"
  cd $GOTODIR
  CURRENTDIR=`pwd`

  if [ "$CURRENTDIR" = "$GOTODIR" ]; then
    echo ">>> DELETE ALL FILES IN $CURRENTDIR"
    rm -R -f *

    echo ">>> GIT cloneing $CLONE"
    git clone $CLONE
    echo ">>> CHANGE PERMISSIONS IN $CURRENTDIR"
    chmod -R o-rwx "$CLONE_PATH/"
    chmod -R g+rw "$CLONE_PATH/"

#   in CLONE_PATH
    GOTODIR=$CLONE_PATH
    cd $GOTODIR
    CURRENTDIR=`pwd`

    if [ "$CURRENTDIR" = "$GOTODIR" ]; then
      echo ">>> REMOVE NOT NEEDED FILES IN $CURRENTDIR"
      rm -f README.md .gitignore composer.lock symfony.lock .env
      rm -R -f Homestead/ .git/
    else
      echo "ERROR: could not cd to $GOTODIR"
    fi

    GOTODIR="$APPLICATION_PATH/httpdocs"
    cd $GOTODIR
    CURRENTDIR=`pwd`

    if [ "$CURRENTDIR" = "$GOTODIR" ]; then
      echo ">>> CLEANUP HTTPDOCS => REMOVE NOT NEEDED FILES IN $CURRENTDIR"
#     if composer needs to update the composer.json add write permission
      chmod ug+wx composer.json
#     delete all directories below httpdocs
      rm -R -f `find . -maxdepth 1 -type d`
#     delete not needed files in httpdocs
      rm -f *.lock *.log .gitignore
#     do not copy composer.json and .env !!!
      echo ">>> COPYING CLONED FILES TO $CURRENTDIR"
      cp -R "$CLONE_PATH/bin/" .
      cp -R "$CLONE_PATH/config/" .
      cp -R "$CLONE_PATH/public/" .
      cp -R "$CLONE_PATH/src/" .
#     >>>>>>>>>>>>>>  copy DIGISTORE_API ==> this overwritten by composer install !!! => digistore_api should be moved from vendor/ in src/ExternalApis/ in bitbucket <<<<<<<<<<<<<
      cp -R "$CLONE_PATH/vendor/" .
#      cp -R "$CLONE_PATH/templates/" .
      echo ">>> RUNNING COMPOSER INSTALL"
#     1st run normal install to upgrade symfony/flex
      composer install
#     2nd upgrade symfony/flex
      composer upgrade symfony/flex
#     3rd generate the .env.local.php file
      composer dump-env prod
#     4th delete and reinstall vendor for prod
      rm -R -f vendor/ *.lock
      composer install --no-dev --optimize-autoloader
#     >>>>>>>>>>>>>>    4.1 MSDIGI SPECIAL copy /vendor/digistore-api from clone path  <<<<<<<<<<<<<<<<<<
      cp -R "$CLONE_PATH/vendor/digistore-api" "$APPLICATION_PATH/httpdocs/vendor"
      composer update --no-dev --optimize-autoloader
#     5th clear and warm-up Symfony cache
#      APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear --no-warmup
      APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
#     6th remove write permission from composer.json
      chmod ug-wx composer.json
#     7th copy the htaccessForPublic and change .htaccess permission to -rwxr-xr--, change permission of folder public to rwxr-x--x
      cp $HTACCESSFORPUBLIC "$APPLICATION_PATH/httpdocs/public/.htaccess"
      chmod 754 "$APPLICATION_PATH/httpdocs/public/.htaccess"
      chmod 751 "$APPLICATION_PATH/httpdocs/public"
#     8th copy robots.txt to public/
      cp "$ROBOTSTXT" "$APPLICATION_PATH/httpdocs/public/robots.txt"
      chmod 555 "$APPLICATION_PATH/httpdocs/public/robots.txt"
      php bin/console debug:router
    else
      echo "ERROR: could not cd to $GOTODIR"
    fi

  else
      echo "ERROR: could not cd to $GOTODIR"
  fi

else
    echo "ERROR: WRONG USER $USER1. LOGIN AS treaction-kto206-api"
fi
